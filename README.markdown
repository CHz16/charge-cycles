Charge Cycles
=============

A puzzle game made for [Strawberry Jam 2](https://itch.io/jam/strawberry-jam-2). Playable here: https://chz.itch.io/charge-cycles

Everything here is by me and released under MIT (see LICENSE.txt), except:

* Music by [Chimeratio](https://soundcloud.com/chimeratio)! [The soundtrack is available on SoundCloud](https://soundcloud.com/chimeratio/sets/charge-cycles-ost) for listening outside of the game.
* The `extend` sound effect, which is edited from ["56k Modem.mp3" by BlueNeon from Freesound](https://freesound.org/people/BlueNeon/sounds/203512/) ([CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/))
* The Ulka logo (`credits.png` and `manual-1-1-1.png`), which uses ["meteor" by Aldric Rodríguez from the Noun Project](https://thenounproject.com/term/meteor/777675/) ([CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/))
* Numeric springing code by Allen Chou, released under MIT (see `charge-cycles/numbers.js`)
