// clientsscene.js
// Controller for the clients scene.

let CLIENTS_ROW_HEIGHT = 50, CLIENTS_SCROLLER_WIDTH = 40;

function ClientsSceneController() {
    this.clearRowButtons = function() {
        for (let i = 0; i < this.rows.length; i++) {
            removeButton(this.rows[i].button);
        }
    }

    this.updateButtons = function() {
        this.clearRowButtons();

        let contentHeight = levels.length * CLIENTS_ROW_HEIGHT + 2;
        let visibleTop = this.scrollPosition * (contentHeight - (tabletScreenHeight - 31));
        let firstVisibleRow = Math.floor(visibleTop / CLIENTS_ROW_HEIGHT);

        let y = -(visibleTop % CLIENTS_ROW_HEIGHT) + 31, currentRow = firstVisibleRow;
        if (y > 31) {
            y -= CLIENTS_ROW_HEIGHT;
        }
        while (y <= tabletScreenHeight && currentRow < this.rows.length) {
            if (currentRow >= 0) {
                let button = this.rows[currentRow].button;
                button.top = Math.max(y, 31);
                button.bottom = Math.min(y + CLIENTS_ROW_HEIGHT, tabletScreenHeight);
                addButton(button);
            }

            y += CLIENTS_ROW_HEIGHT;
            currentRow += 1;
        }
    }

    this.setScrollTarget = function(y) {
        this.scrollTarget = (y - 5 - this.scroller.top) / (this.scroller.bottom - this.scroller.top - 10);
        this.scrollTarget = clamp(this.scrollTarget, 0, 1);
    }


    this.leaving = undefined;

    this.rows = [], this.activeRow = undefined;
    for (let i = 0; i < levels.length; i++) {
        this.rows.push({
            // label and solved properties are set in startTransitionIn to refresh them
            button: new Button(
                0, 0, tabletScreenWidth - CLIENTS_SCROLLER_WIDTH, 50, // y and height will be set dynamically after scrolling
                {
                    mouseDown: function(x, y) { this.activeRow = i; }.bind(this),
                    mouseUp: function(x, y) { scenes["game"].loadLevel(i); startTransition("game", Transition.rotateIn); }.bind(this),
                    mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeRow = i; } }.bind(this),
                    mouseLeave: function(x, y, mouseIsDown) { this.activeRow = undefined; }.bind(this)
                }
            )
        });
        this.rows[i].button.level = i;
    }

    // We want to update buttons even if the user mouse ups outside of the slider, so we need to trap this too
    this.mouseUp = function(x, y) {
        if (this.scrolling) {
            this.updateButtons();
        }
        this.scrolling = false;
    }
    this.scrollPosition = 0, this.scrollTarget = undefined, this.scrollVelocity = undefined;
    this.scrolling = false;
    this.scroller = new Button(
        tabletScreenWidth - CLIENTS_SCROLLER_WIDTH, 30, CLIENTS_SCROLLER_WIDTH, tabletScreenHeight - 30,
        {
            cursor: "grab",
            cursorDown: "grabbing",
            mouseDown: function(x, y) { this.scrolling = true; this.setScrollTarget(y); }.bind(this),
            mouseDrag: function(x, y) { this.setScrollTarget(y); }.bind(this),
            mouseUp: function(x, y) { this.scrolling = false; this.updateButtons(); }.bind(this)
        }
    );
}

ClientsSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    tabletContext.fillStyle = "gray";
    tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

    let springValues = numericSpring(this.scrollPosition, this.scrollVelocity, this.scrollTarget, 4 * Math.PI, 0.5, dt);
    this.scrollPosition = springValues.x;
    this.scrollVelocity = springValues.v;
    if (!this.leaving) {
        this.updateButtons();
    }

    let contentHeight = levels.length * CLIENTS_ROW_HEIGHT + 2;
    let visibleTop = this.scrollPosition * (contentHeight - (tabletScreenHeight - 31));
    let firstVisibleRow = Math.floor(visibleTop / CLIENTS_ROW_HEIGHT);

    tabletContext.strokeStyle = "black";
    tabletContext.lineWidth = 2;
    tabletContext.font = "30px sans-serif";
    let y = -(visibleTop % CLIENTS_ROW_HEIGHT) + 31, currentRow = firstVisibleRow;
    if (y > 31) {
        y -= CLIENTS_ROW_HEIGHT;
    }
    while (y <= tabletScreenHeight && currentRow < this.rows.length) {
        if (currentRow >= 0) {
            let row = this.rows[currentRow];

            tabletContext.fillStyle = ((this.activeRow === currentRow) ? "#A0A0A0" : "#D0D0D0");
            tabletContext.fillRect(0, y, tabletScreenWidth - CLIENTS_SCROLLER_WIDTH, CLIENTS_ROW_HEIGHT);
            tabletContext.strokeRect(0, y, tabletScreenWidth - CLIENTS_SCROLLER_WIDTH, CLIENTS_ROW_HEIGHT);

            tabletContext.fillStyle = "black";
            drawText(tabletContext, row.label, 10, y + 11, 30);

            if (row.solved) {
                tabletContext.drawImage(imagePreloader.getImage("check"), tabletScreenWidth - CLIENTS_SCROLLER_WIDTH - 45, y + 7);
            }
        }

        y += CLIENTS_ROW_HEIGHT;
        currentRow += 1;
    }

    tabletContext.strokeStyle = "black";
    tabletContext.lineWidth = 3;
    tabletContext.fillStyle = "white";
    tabletContext.fillRect(this.scroller.left + 2.5, this.scroller.top + 1.5 + this.scrollPosition * (this.scroller.bottom - this.scroller.top - 10 - 5), CLIENTS_SCROLLER_WIDTH, 10);
    tabletContext.strokeRect(this.scroller.left + 2.5, this.scroller.top + 1.5 + this.scrollPosition * (this.scroller.bottom - this.scroller.top - 10 - 5), CLIENTS_SCROLLER_WIDTH - 7, 10);

    drawStatusBar(tabletContext, "Clients", true);
}

ClientsSceneController.prototype.startTransitionIn = function(transitionType) {
    this.leaving = false;
    this.activeRow = undefined;
    // We'll leave the scroll position where the player left it, clamped to normal boundaries, though we will stop the scroll movement
    this.scrollPosition = clamp(this.scrollPosition, 0, 1);
    this.scrollTarget = this.scrollPosition;
    this.scrollVelocity = 0;
    this.scrolling = false;

    let solved = defaults.get("solved");
    let robots = defaults.get("robots");
    for (let i = 0; i < this.rows.length; i++) {
        this.rows[i].solved = solved[i];
        this.rows[i].label = "[" + ((i < 9) ? "0" : "") + (i + 1) + "] " + robots[i].name;
    }
}

ClientsSceneController.prototype.finishTransitionIn = function(transitionType) {
    this.updateButtons();
    addButton(this.scroller);
}

ClientsSceneController.prototype.startTransitionOut = function(transitionType) {
    this.leaving = true;
    removeButton(this.scroller);
    this.clearRowButtons();
}

ClientsSceneController.prototype.finishTransitionOut = function(transitionType) {

}
