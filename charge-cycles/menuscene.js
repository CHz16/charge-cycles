// menuscene.js
// Controller for the menu scene.

function MenuSceneController() {
    this.mouseDownInButton = function(i) {
        this.activeButton = i;
    }
    this.mouseUpInButton = function(i) {
        startTransition(this.buttons[i].destination, this.buttons[i].transition);
    }
    this.mouseEnterButton = function(i, mouseIsDown) {
        if (mouseIsDown) {
            this.activeButton = i;
        }
    }
    this.mouseLeaveButton = function(i, mouseIsDown) {
        this.activeButton = undefined;
    }


    this.backgroundCanvas = document.createElement("canvas");
    this.backgroundCanvas.width = tabletScreenWidth;
    this.backgroundCanvas.height = tabletScreenHeight;
    this.backgroundCanvasContext = this.backgroundCanvas.getContext("2d");

    let buttonDefinitions = [
        {x: 59, y: 30+45, icon: "clients", name: "Clients", destination: "clients", transition: Transition.tabletFade},
        {x: 59+100+59+100+59, y: 30+45, icon: "musicplayer", name: "Music Player", destination: "musicplayer", transition: Transition.tabletFade},
        {x: 59+100+59+100+59+100+59, y: 30+45, icon: "settings", name: "Settings", destination: "settings", transition: Transition.tabletFade},
        {x: 59, y: 30+45+115+45, icon: "manual", name: "Manual", destination: "manual", transition: Transition.tabletFade},
        {x: 59+100+59+100+59+100+59, y: 30+45+115+45, icon: "credits", name: "Credits", destination: "credits", transition: Transition.tabletFade},
        {x: 300, y: 397, icon: "backtoclient", name: "Back to Client", destination: "game", transition: Transition.rotateIn}
    ];

    this.activeButton = undefined;
    this.buttons = [];
    for (let i = 0; i < buttonDefinitions.length; i++) {
        let def = buttonDefinitions[i];
        let button = new Button(
            def.x, def.y, 100, 115,
            {
                mouseDown: function(x, y) { this.mouseDownInButton(i); }.bind(this),
                mouseUp: function(x, y) { this.mouseUpInButton(i); }.bind(this),
                mouseEnter: function(x, y, mouseIsDown) { this.mouseEnterButton(i, mouseIsDown); }.bind(this),
                mouseLeave: function(x, y, mouseIsDown) { this.mouseLeaveButton(i, mouseIsDown); }.bind(this),
            },
        );
        button.icon = def.icon;
        button.name = def.name;
        button.destination = def.destination;
        button.transition = def.transition;
        this.buttons.push(button);
    }
    this.backToClientButton = this.buttons[5];

    this.unsolved = undefined;
}

MenuSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    tabletContext.drawImage(this.backgroundCanvas, 0, 0);

    // Active app backdrop
    if (this.activeButton !== undefined) {
        let button = this.buttons[this.activeButton];
        tabletContext.fillStyle = "rgba(255, 255, 255, 0.5)";
        tabletContext.fillRect(button.left, button.top, button.right - button.left + 1, button.bottom - button.top + 1);
    }

    // App icons and labels
    tabletContext.font = "16px sans-serif";
    for (let i = 0; i < this.buttons.length; i++) {
        let button = this.buttons[i];
        if ((scenes["game"].currentLevel === undefined || scenes["game"].victoryAnimationState !== undefined) && button.id == this.backToClientButton.id) {
            continue;
        }
        tabletContext.fillStyle = (defaults.get("background") != 3 || button.left > 100) ? "white" : "black";
        tabletContext.drawImage(imagePreloader.getImage(button.icon), button.left + 10, button.top + 10);
        drawCenteredText(tabletContext, button.name, (button.left + button.right) / 2, button.top + 95, 16);
    }

    // Clients app badge
    let clientsBadgeCenterX = this.buttons[0].right - 12, clientsBadgeCenterY = this.buttons[0].top + 12;
    if (this.unsolved > 0) {
        tabletContext.fillStyle = "red";
        tabletContext.beginPath();
        tabletContext.moveTo(clientsBadgeCenterX, clientsBadgeCenterY);
        tabletContext.arc(clientsBadgeCenterX, clientsBadgeCenterY, 14, 0, 2 * Math.PI);
        tabletContext.fill();

        tabletContext.fillStyle = "white";
        tabletContext.font = "20px sans-serif";
        if (this.unsolved < 10) {
            drawCenteredText(tabletContext, this.unsolved, clientsBadgeCenterX + 1, clientsBadgeCenterY - 8, 20);
        } else {
            drawCenteredText(tabletContext, this.unsolved, clientsBadgeCenterX, clientsBadgeCenterY - 7, 20);
        }
    } else {
        tabletContext.drawImage(imagePreloader.getImage("star"), clientsBadgeCenterX - 16, clientsBadgeCenterY - 14);
    }

    drawStatusBar(tabletContext, "Charge Cycles", true);
}

MenuSceneController.prototype.startTransitionIn = function(transitionType) {
    this.activeButton = undefined;

    if (isSafari) {
        // Safari doesn't support context.filter yet, so I guess I have to premake blurred backgrounds lmao
        this.backgroundCanvasContext.drawImage(imagePreloader.getImage("background" + defaults.get("background") + "-blurred"), 3, 30);
    } else {
        // Pregenerate the background with a blurred bottom area
        this.backgroundCanvasContext.fillStyle = "black";
        this.backgroundCanvasContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

        // There's an extra 10px on the left and right of the images because the CSS blur makes a fuzzy border around the edges.
        // So our images are extra large and we just cut that shit off.
        this.backgroundCanvasContext.filter = "blur(10px)";
        this.backgroundCanvasContext.drawImage(imagePreloader.getImage("background" + defaults.get("background")), -10, 0);
        this.backgroundCanvasContext.filter = "none";
        this.backgroundCanvasContext.drawImage(imagePreloader.getImage("background" + defaults.get("background")), 0, 0, tabletScreenWidth + 20, tabletScreenHeight - 125, -10, 0, tabletScreenWidth + 20, tabletScreenHeight - 125);
    }

    if (scenes["game"].currentLevel !== undefined && scenes["game"].victoryAnimationState === undefined) {
        this.backToClientButton.name = defaults.get("robots")[scenes["game"].currentLevel].name;
    }

    let solved = defaults.get("solved");
    this.unsolved = 0;
    for (let i = 0; i < solved.length; i++) {
        if (!solved[i]) {
            this.unsolved += 1;
        }
    }

    let menuSong = (this.unsolved > 0) ? "menu-full" : "menu2-full";
    if (musicPlayer.currentSong() !== menuSong) {
        musicPlayer.play(menuSong);
    }
}

MenuSceneController.prototype.finishTransitionIn = function(transitionType) {
    addButtons(this.buttons);
    if (scenes["game"].currentLevel === undefined || scenes["game"].victoryAnimationState !== undefined) {
        removeButton(this.backToClientButton);
    }
}

MenuSceneController.prototype.startTransitionOut = function(transitionType) {
    removeButtons(this.buttons);
}

MenuSceneController.prototype.finishTransitionOut = function(transitionType) {
    this.activeButton = undefined;
}

