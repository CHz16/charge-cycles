// gamescene.js
// Controller for the game scene.

let GAME_TILE_SIZE = 30, GAME_TILE_INSET_SIZE = 5;
let GAME_ROBOT_GRID_PADDING = 10;
let GAME_MOVEMENT_DURATION = 150; // milliseconds
let GAME_TABLET_SLIVER_WIDTH = 294;
let GAME_TURN_LIMIT = 50;

let GAME_ZOOM_SCALE = 4;
let GAME_SHAKE_MAX_AMPLITUDE = 5;
let GAME_EYES_FLASH_INTERVAL = 200; // milliseconds

let VictoryAnimationState = {speedup: 0, pauseBeforeZoom: 1, zoom: 2, pauseAfterZoom: 3, cooldown: 4, pauseAfterCooldown: 5, extendingItem: 6, showingReviewBox: 7, fadingReviewContents: 8, reviewing: 9};
// 1250 + 600 = 1850, synced-ish with the extend sound effect
let GAME_VICTORY_ANIMATION_STATE_DURATIONS = [1500, 1000, 2500, 2000, 1250, 600, 50, 250, 250]; // milliseconds
let GAME_VICTORY_ANIMATION_MIN_LOOPS = 5;

let PlaybackState = {planning: 0, retracting: 1, extending: 2, reviewing: 3};

function GameSceneController() {
    this.nextLoopedIndex = function(i) {
        if (this.loopStart === undefined) {
            return i + 1;
        } else {
            return (i + 1 - this.loopStart) % (this.playbackHistory.length - this.loopStart) + this.loopStart;
        }
    }

    this.makeCompositeCanvas = function(imageThings, width, height) {
        let canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        let canvasContext = canvas.getContext("2d");

        let maskCanvas = document.createElement("canvas");
        maskCanvas.width = width;
        maskCanvas.height = height;
        let maskCanvasContext = maskCanvas.getContext("2d");

        for (let i = 0; i < imageThings.length; i++) {
            maskCanvasContext.globalCompositeOperation = "source-over";
            maskCanvasContext.clearRect(0, 0, width, height);
            maskCanvasContext.drawImage(imagePreloader.getImage(imageThings[i].image), 0, 0);
            maskCanvasContext.globalCompositeOperation = "source-in";
            maskCanvasContext.fillStyle = imageThings[i].color;
            maskCanvasContext.fillRect(0, 0, width, height);
            canvasContext.drawImage(maskCanvas, 0, 0);
        }

        return canvas;
    }

    this.makePrismCanvas = function(width, height, insetSize, color, shadeColor) {
        let canvas = document.createElement("canvas");
        canvas.width = width + insetSize;
        canvas.height = height + insetSize;
        let canvasContext = canvas.getContext("2d");

        // sides
        canvasContext.fillStyle = shadeColor;
        canvasContext.beginPath();
        canvasContext.moveTo(insetSize + 1, 0);
        canvasContext.lineTo(insetSize + width, 0);
        canvasContext.lineTo(insetSize + width, height - 1);
        canvasContext.lineTo(width, height + insetSize - 1);
        canvasContext.lineTo(1, insetSize);
        canvasContext.closePath();
        canvasContext.fill();

        // front
        canvasContext.fillStyle = color;
        canvasContext.fillRect(0, insetSize, width, height);

        // lines
        canvasContext.strokeStyle = "black";
        canvasContext.lineWidth = 2;
        canvasContext.beginPath();
        canvasContext.rect(1, insetSize + 1, width - 2, height - 2);
        canvasContext.moveTo(1, insetSize + 1);
        canvasContext.lineTo(insetSize + 1, 1);
        canvasContext.lineTo(width + insetSize - 1, 1);
        canvasContext.lineTo(width + insetSize - 1, height - 1);
        canvasContext.lineTo(width - 1, height + insetSize - 1);
        canvasContext.moveTo(width + insetSize - 1, 1);
        canvasContext.lineTo(width - 1, insetSize + 1);
        canvasContext.stroke();

        return canvas;
    }

    this.currentLevel = undefined;
    this.needsRefresh = false;

    this.blockCanvas = undefined;
    this.robotCanvas = undefined;
    this.robotLeft = undefined;
    this.robotTop = undefined;
    this.headLeft = undefined;

    this.gridLeft = undefined;
    this.gridWidth = undefined;
    this.gridTop = undefined;
    this.gridHeight = undefined;

    this.leaving = false;
    this.gameState = undefined, this.staticGrid = undefined, this.objectGrid = undefined;
    this.nextState = undefined, this.nextGameState = undefined, this.nextUpdateTime = undefined, this.playbackState = undefined;
    this.playbackHistory = undefined, this.stateLookupTable = undefined, this.currentStep = undefined, this.haltReason = undefined, this.loopStart = undefined;

    this.energizeButton = new Button(
        536, 398, 80, 80,
        {
            mouseDown: function(x, y) { this.activeButton = 0; }.bind(this),
            mouseUp: function(x, y) {
                this.nextUpdateTime = -1;
                this.playbackState = PlaybackState.extending;
                if (this.playbackHistory === undefined) {
                    this.gameState = this.initialGameState;
                    this.playbackHistory = [{state: this.initialGameState, objectGrid: this.objectGrid}];
                    this.stateLookupTable = {};
                    this.stateLookupTable[this.hashState(this.initialGameState)] = 0;
                    this.currentStep = 0;
                }
                if (this.haltReason === HaltReason.paused) {
                    this.haltReason = undefined;
                }
                this.activeButton = undefined;
                removeButton(this.energizeButton);
                removeButton(this.plotButton);
                addButton(this.pauseButton);
                addButton(this.restartButton);
                removeButton(this.stepBackwardButton);
                removeButton(this.stepForwardButton);
                removeButtons(this.stateSelectButtons);
                musicPlayer.play("ingame" + levels[this.currentLevel].music + "-full", {sync: true});
            }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeButton = 0; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.activeButton = undefined; }.bind(this)
        }
    );
    this.energizeButton.image = "play";

    this.switchToReview = function() {
        this.nextGameState = undefined;
        this.nextObjectGrid = undefined;
        this.nextUpdateTime = undefined;
        this.playbackState = PlaybackState.reviewing;
        this.activeButton = undefined;
        removeButton(this.pauseButton);
        addButton(this.energizeButton);
        addButton(this.stepBackwardButton);
        addButton(this.stepForwardButton);
        this.toggleReviewButtons();
        addButtons(this.stateSelectButtons.slice(0, this.playbackHistory.length));
        removeButton(this.stateSelectButtons[this.currentStep]);
        if (!this.leaving) {
            musicPlayer.play("ingame" + levels[this.currentLevel].music + "-light", {sync: true});
        }
    }
    this.pauseButton = new Button(
        536, 398, 80, 80,
        {
            mouseDown: function(x, y) { this.activeButton = 1; }.bind(this),
            mouseUp: function(x, y) {
                if (this.haltReason === undefined) {
                    this.haltReason = HaltReason.paused;
                }
                this.switchToReview();
            }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeButton = 1; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.activeButton = undefined; }.bind(this)
        }
    );
    this.pauseButton.image = "pause";

    this.restartButton = new Button(
        536, 274, 80, 80,
        {
            mouseDown: function(x, y) { this.activeButton = 2; }.bind(this),
            mouseUp: function(x, y) {
                this.gameState = this.initialGameState;
                this.nextGameState = undefined;
                this.objectGrid = this.makeObjectGrid(this.staticGrid, this.gameState);
                this.playbackState = PlaybackState.planning;
                this.playbackHistory = undefined;
                this.stateLookupTable = undefined;
                this.currentStep = undefined;
                this.haltReason = undefined;
                this.loopStart = undefined;
                this.activeButton = undefined;
                removeButton(this.restartButton);
                removeButton(this.pauseButton);
                removeButton(this.stepBackwardButton);
                removeButton(this.stepForwardButton);
                removeButtons(this.stateSelectButtons);
                addButton(this.energizeButton);
                addButton(this.plotButton);
                this.energizeButton.enabled = true;
            }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeButton = 2; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.activeButton = undefined; }.bind(this)
        }
    );
    this.restartButton.image = "restart";

    this.toggleReviewButtons = function() {
        this.stepBackwardButton.enabled = (this.loopStart === 0 || this.currentStep != 0);
        this.stepForwardButton.enabled = (this.currentStep != this.playbackHistory.length - 1 || this.loopStart !== undefined);
        this.energizeButton.enabled = (this.haltReason === HaltReason.paused || this.currentStep != this.playbackHistory.length - 1 || this.loopStart !== undefined);
    }
    this.switchToStep = function(step) {
        addButton(this.stateSelectButtons[this.currentStep]);
        this.currentStep = step;
        this.gameState = this.playbackHistory[this.currentStep].state;
        this.objectGrid = this.playbackHistory[this.currentStep].objectGrid;
        removeButton(this.stateSelectButtons[this.currentStep]);
        this.toggleReviewButtons();
    }
    this.stepBackwardButton = new Button(
        412, 398, 80, 80,
        {
            mouseDown: function(x, y) { this.activeButton = 3; }.bind(this),
            mouseUp: function(x, y) {
                this.switchToStep(this.currentStep == 0 ? this.playbackHistory.length - 1 : this.currentStep - 1);
                this.activeButton = undefined;
            }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeButton = 3; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.activeButton = undefined; }.bind(this)
        }
    );
    this.stepBackwardButton.image = "stepbackward";
    this.stepForwardButton = new Button(
        412, 274, 80, 80,
        {
            mouseDown: function(x, y) { this.activeButton = 4; }.bind(this),
            mouseUp: function(x, y) {
                this.switchToStep(this.nextLoopedIndex(this.currentStep));
                this.activeButton = undefined;
            }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeButton = 4; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.activeButton = undefined; }.bind(this)
        }
    );
    this.stepForwardButton.image = "stepforward";

    this.activeStateSelectButton = undefined, this.stateSelectTarget = undefined;
    this.stateSelectButtons = [];
    let stateSelectRow = 0, stateSelectCol = 0;
    for (let i = 0; i < GAME_TURN_LIMIT; i++) {
        let x = 588 + stateSelectCol * 21, y = 108 + stateSelectRow * 37;
        this.stateSelectButtons.push(new Button(
            x, y, 15, 31,
            {
                onTabletScreen: false,
                mouseDown: function(x, y) { this.activeStateSelectButton = i; }.bind(this),
                mouseUp: function(x, y) {
                    this.stateSelectTarget = i;
                    this.activeStateSelectButton = undefined;
                }.bind(this),
                mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeStateSelectButton = i; } }.bind(this),
                mouseLeave: function(x, y, mouseIsDown) { this.activeStateSelectButton = undefined; }.bind(this)
            }
        ));
        stateSelectCol += 1;
        if (stateSelectCol == 10) {
            stateSelectCol = 0;
            stateSelectRow += 1;
        }
    }

    this.plottingCharger = false, this.plottingChargerByDragging = false, this.hoveredCell = undefined; this.highlightedCell = undefined;
    this.inProgressCharger = undefined, this.currentPath = undefined, this.validNextCells = undefined;
    this.highlightedCharger = undefined;
    this.remainingChargers = undefined;
    this.plotButtonMoveCallback = function(x, y, mouseIsDown) {
        let row = Math.floor((y - this.plotButton.top) / GAME_TILE_SIZE);
        let col = Math.floor((x - this.plotButton.left) / GAME_TILE_SIZE);

        if (!this.plottingCharger && (this.hoveredCell === undefined || (row !== this.hoveredCell.row || col !== this.hoveredCell.col))) {
            this.hoveredCell = undefined;
            this.highlightedCell = undefined;
            this.highlightedCharger = undefined;

            if (row >= 0 && row < this.objectGrid.length && col >= 0 && col < this.objectGrid[0].length) {
                this.hoveredCell = {row: row, col: col};
            } else {
                return;
            }

            let totalRemainingChargers = Object.values(this.remainingChargers).reduce((acc, cur) => acc + cur, 0);
            if (totalRemainingChargers > 0 && this.objectGrid[row][col] === GridObject.empty) {
                this.highlightedCell = this.hoveredCell;
            } else if (typeof this.objectGrid[row][col] == "object") {
                for (let i = 0; i < this.initialGameState.chargers.length; i++) {
                    let charger = this.initialGameState.chargers[i];
                    let chargerRow = charger.head.row, chargerCol = charger.head.col;
                    if (row == chargerRow && col == chargerCol) {
                        this.highlightedCharger = i;
                        return;
                    }
                    for (let j = 0; j < charger.tail.length; j++) {
                        chargerRow += movementDeltas[charger.tail[j]].row;
                        chargerCol += movementDeltas[charger.tail[j]].col;
                        if (row == chargerRow && col == chargerCol) {
                            this.highlightedCharger = i;
                            return;
                        }
                    }
                }
            }
        } else if (this.plottingCharger) {
            // Just do a check to see if the user is dragging, and update our records if so
            if (mouseIsDown && (this.inProgressCharger.head.row != row || this.inProgressCharger.head.col != col)) {
                this.plottingChargerByDragging = true;
            }

            // Check if the user dragged to a valid cell to extend the charger
            let caboose = this.currentPath[this.currentPath.length - 1];
            if (row != caboose.row || col != caboose.col) {
                let movedToValidCell = false;
                for (let i = 0; i < this.validNextCells.length; i++) {
                    if (row == this.validNextCells[i].row && col == this.validNextCells[i].col) {
                        movedToValidCell = true;
                        break;
                    }
                }
                if (movedToValidCell) {
                    let direction;
                    if (row == caboose.row - 1) {
                        direction = Direction.up;
                    } else if (row == caboose.row + 1) {
                        direction = Direction.down;
                    } else if (col == caboose.col - 1) {
                        direction = Direction.left;
                    } else {
                        direction = Direction.right;
                    }
                    this.inProgressCharger.tail.push(direction);
                    this.currentPath.push({row: row, col: col});
                    this.objectGrid = this.makeObjectGrid(this.staticGrid, this.initialGameState);
                    this.findValidNextCells();
                    soundPlayer.play("click");
                    return;
                }
            }

            // Check if the user backed up to a previous cell
            for (let i = 0; i < this.currentPath.length - 1; i++) {
                if (row == this.currentPath[i].row && col == this.currentPath[i].col) {
                    this.inProgressCharger.tail = this.inProgressCharger.tail.slice(0, i);
                    this.currentPath = this.currentPath.slice(0, i + 1);
                    this.objectGrid = this.makeObjectGrid(this.staticGrid, this.initialGameState);
                    this.findValidNextCells();
                    soundPlayer.play("click");
                    return;
                }
            }

            // Invalid - we'll just allow this by not doing anything
        }
    };
    this.findValidNextCells = function() {
        this.validNextCells = [];

        let longestRemainingLength = 0;
        let lengths = Object.keys(this.remainingChargers).sort().reverse();
        for (let i = 0; i < lengths.length; i++) {
            if (this.remainingChargers[lengths[i]] > 0) {
                longestRemainingLength = lengths[i];
                break;
            }
        }
        if (this.currentPath.length == longestRemainingLength) {
            return;
        }

        let caboose = this.currentPath[this.currentPath.length - 1];
        let probes = [
            {row: caboose.row - 1, col: caboose.col},
            {row: caboose.row + 1, col: caboose.col},
            {row: caboose.row, col: caboose.col - 1},
            {row: caboose.row, col: caboose.col + 1},
        ];
        for (let i = 0; i < probes.length; i++) {
            let probe = probes[i];
            if (probe.row >= 0 && probe.row < this.objectGrid.length && probe.col >= 0 && probe.col < this.objectGrid[0].length) {
                if (this.objectGrid[probe.row][probe.col] === GridObject.empty) {
                    this.validNextCells.push(probe);
                }
            }
        }
    }
    this.stopPlottingCharger = function(validate) {
        if (validate && this.remainingChargers[this.currentPath.length] > 0) {
            this.energizeButton.enabled = true;
            this.remainingChargers[this.currentPath.length] -= 1;
            this.inProgressCharger.ghost = false;
            let solutions = defaults.get("solutions");
            solutions[this.currentLevel] = this.initialGameState;
            defaults.set("solutions", solutions);
            soundPlayer.play("plop");
        } else {
            this.initialGameState.chargers.pop();
        }

        this.plottingCharger = false;
        this.plottingChargerByDragging = false;
        this.inProgressCharger = undefined;
        this.currentPath = undefined;
        this.validNextCells = undefined;
        this.objectGrid = this.makeObjectGrid(this.staticGrid, this.initialGameState);
    }
    this.plotButton = new Button(
        0, 0, 1, 1, // bounds set in finishTransitionIn based on grid size
        {
            cursor: "cell",
            cursorGrab: "cell",
            onTabletScreen: false,
            mouseDown: function(x, y) {
                if (!this.plottingCharger) {
                    if (this.highlightedCell !== undefined) {
                        // Start a charger
                        this.plottingCharger = true;
                        this.inProgressCharger = {head: this.highlightedCell, tail: [], ghost: true};
                        this.initialGameState.chargers.push(this.inProgressCharger);
                        this.objectGrid = this.makeObjectGrid(this.staticGrid, this.initialGameState);
                        this.currentPath = [this.highlightedCell];
                        this.findValidNextCells();
                        this.highlightedCell = undefined;
                        this.hoveredCell = undefined; // force a rehighlight
                        soundPlayer.play("click");
                    } else if (this.highlightedCharger !== undefined) {
                        this.remainingChargers[this.initialGameState.chargers[this.highlightedCharger].tail.length + 1] += 1;
                        this.initialGameState.chargers.splice(this.highlightedCharger, 1);
                        let solutions = defaults.get("solutions");
                        solutions[this.currentLevel] = this.initialGameState;
                        defaults.set("solutions", solutions);
                        if (this.initialGameState.chargers.length == 0) {
                            this.energizeButton.enabled = false;
                        }
                        this.objectGrid = this.makeObjectGrid(this.staticGrid, this.initialGameState);
                        this.highlightedCharger = undefined;
                        this.hoveredCell = undefined; // force a rehighlight
                        this.plotButtonMoveCallback(x, y, false);
                        soundPlayer.play("remove");
                    }
                } else {
                    this.stopPlottingCharger(true);
                    this.plotButtonMoveCallback(x, y, false);
                }
            }.bind(this),
            mouseUp: function(x, y) {
                if (this.plottingChargerByDragging) {
                    this.stopPlottingCharger(true);
                    this.plotButtonMoveCallback(x, y, false);
                }
            }.bind(this),
            mouseMove: function(x, y) { this.plotButtonMoveCallback(x, y, false); }.bind(this),
            mouseDrag: function(x, y) { this.plotButtonMoveCallback(x, y, true); }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) {
                if (!this.plottingCharger) {
                    this.hoveredCell = undefined;
                    this.highlightedCell = undefined;
                    this.highlightedCharger = undefined;
                } else {
                    this.stopPlottingCharger(false);
                }
            }.bind(this)
        }
    );

    this.activeButton = undefined;
    this.buttons = [this.energizeButton, this.pauseButton, this.restartButton, this.stepBackwardButton, this.stepForwardButton, this.plotButton];
    this.buttonIDs = [];
    for (let i = 0; i < this.buttons.length; i++) {
        this.buttonIDs.push(this.buttons[i].id);
    }

    this.atMaxSpeed = false;
    this.victoryAnimationState = undefined, this.victoryAnimationStateChangeTime = undefined, this.victoryChargeLoops = undefined;
    this.eyesXOffset = undefined, this.eyesYOffset = undefined;
    this.eyesFlashing = false, this.eyesFlashToggleTime = undefined, this.eyesFlashCanvas = undefined;
    this.itemRecoilY = undefined, this.itemRecoilVelocity = undefined;
    this.headCanvas = undefined, this.reviewStartTime = undefined, this.reviewMessage = undefined;

    this.reviewLineMaxLength = 0;
    let reviewMaxWidth = GAME_TABLET_SLIVER_WIDTH - 2 * 10 - 2 * 6;
    tabletCanvasContext.font = "25px sans-serif";
    let measurementReview = "", metrics;
    do {
        measurementReview += "\u286f";
        this.reviewLineMaxLength += 1;
        metrics = tabletCanvasContext.measureText(measurementReview);
    } while (metrics.width <= reviewMaxWidth)
    this.reviewLineMaxLength -= 3; // 1 extra and 2 delimiters
}

GameSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    // When charging, schedule animations and such
    // The first step will also only advance once the chargers have looped a set number of times, so longer cycles aren't cut off so early as to be unsatisfying
    if (this.victoryAnimationStateChangeTime !== undefined && timestamp >= this.victoryAnimationStateChangeTime && this.victoryChargeLoops >= GAME_VICTORY_ANIMATION_MIN_LOOPS) {
        this.victoryAnimationState += 1;
        this.victoryAnimationStateChangeTime = timestamp + GAME_VICTORY_ANIMATION_STATE_DURATIONS[this.victoryAnimationState];

        if (this.victoryAnimationState === VictoryAnimationState.pauseBeforeZoom) {
            // If the eyes didn't start flashing during speedup, then start flashing them now
            if (this.eyesFlashToggleTime === undefined) {
                this.eyesFlashToggleTime = timestamp + GAME_EYES_FLASH_INTERVAL;
                this.eyesFlashing = true;
            }
        } else if (this.victoryAnimationState === VictoryAnimationState.zoom) {
            // Technically, there's a bug here and in the next branch where this can execute during the transition back to the main menu and cause the menu theme to play at 1.3x (or 2.5x) speed. This would be very easy to fix with a !this.leaving conditional check, as it does two branches down to prevent the menu music from stopping in the same situation. However, I'm leaving these instances unfixed because they're funny
            musicPlayer.setPlaybackRateOfCurrentChannels(1.3, GAME_VICTORY_ANIMATION_STATE_DURATIONS[VictoryAnimationState.zoom] / 1000);
        } else if (this.victoryAnimationState === VictoryAnimationState.pauseAfterZoom) {
            musicPlayer.setPlaybackRateOfCurrentChannels(2.5, GAME_VICTORY_ANIMATION_STATE_DURATIONS[VictoryAnimationState.pauseAfterZoom] / 1000);
        } else if (this.victoryAnimationState === VictoryAnimationState.cooldown) {
            if (!this.leaving) {
                musicPlayer.stop(GAME_VICTORY_ANIMATION_STATE_DURATIONS[VictoryAnimationState.cooldown] / 1000);
                soundPlayer.play("extend");
            }
        } else if (this.victoryAnimationState === VictoryAnimationState.pauseAfterCooldown) {
            removeButton(this.pauseButton);
            removeButton(this.restartButton);
        } else if (this.victoryAnimationState === VictoryAnimationState.extendingItem) {
            this.itemRecoilY = 0;
            this.itemRecoilVelocity = 0;
            this.eyesFlashToggleTime = timestamp + GAME_EYES_FLASH_INTERVAL;
            this.eyesFlashing = true;
        } else if (this.victoryAnimationState === VictoryAnimationState.showingReviewBox) {
            // This has to be generated now to determine the height of the box
            this.reviewMessage = [];
            let reviewLines = randomIntInRange(2, 5);
            for (let i = 0; i < reviewLines; i++) {
                this.reviewMessage.push(randomRobotLanguage(randomIntInRange(3, this.reviewLineMaxLength)));
            }
        } else if (this.victoryAnimationState === VictoryAnimationState.fadingReviewContents) {
            // hold
        } else if (this.victoryAnimationState === VictoryAnimationState.reviewing) {
            this.reviewStartTime = timestamp;
        }
    }
    let victoryAnimationStateProgress = undefined;
    if (this.victoryAnimationStateChangeTime !== undefined) {
        victoryAnimationStateProgress = 1 - (this.victoryAnimationStateChangeTime - timestamp) / GAME_VICTORY_ANIMATION_STATE_DURATIONS[this.victoryAnimationState];
    }

    // When scooching, update the playback & game state
    if (this.nextUpdateTime !== undefined && timestamp >= this.nextUpdateTime) {
        let timeToNextUpdate = GAME_MOVEMENT_DURATION;
        if (this.haltReason === HaltReason.charging) {
            if (this.victoryAnimationState > 0 || this.atMaxSpeed) {
                timeToNextUpdate = 0;
            } else {
                timeToNextUpdate *= 1 - victoryAnimationStateProgress;
                this.atMaxSpeed = (timeToNextUpdate <= 0);
            }
            // Once the chargers hit max speed, start flashing
            if (this.victoryAnimationState === VictoryAnimationState.speedup && this.eyesFlashToggleTime === undefined && this.atMaxSpeed) {
                this.eyesFlashToggleTime = timestamp + GAME_EYES_FLASH_INTERVAL;
                this.eyesFlashing = true;
            }
        }

        if (this.playbackState === PlaybackState.retracting) {
            this.gameState = this.nextGameState;
            if (this.nextObjectGrid !== undefined) {
                this.objectGrid = this.nextObjectGrid;
                this.nextObjectGrid = undefined;
            } else {
                this.objectGrid = this.makeObjectGrid(this.staticGrid, this.gameState);
            }
            if (this.loopStart === undefined && this.currentStep == this.playbackHistory.length - 1) {
                this.currentStep += 1;
                this.playbackHistory.push({state: this.gameState, objectGrid: this.objectGrid});
                this.stateLookupTable[this.hashState(this.gameState)] = this.currentStep;
            } else {
                this.currentStep = this.nextLoopedIndex(this.currentStep);
            }
            this.nextGameState = undefined;
            this.playbackState = PlaybackState.extending;
            this.nextUpdateTime = timestamp + timeToNextUpdate;
        } else if (this.playbackState === PlaybackState.extending) {
            if (this.loopStart !== undefined || this.currentStep < this.playbackHistory.length - 1) {
                // If we've previously found a loop or if the user has backtracked and hit play again, then pull the next state from the history instead of making a new one
                // This does duplicate some logic from the next branch too. Jam code!
                if (this.loopStart !== undefined && this.currentStep == this.loopStart) {
                    this.victoryChargeLoops += 1;
                }
                this.nextGameState = this.playbackHistory[this.nextLoopedIndex(this.currentStep)].state;
                if (this.nextGameState.error !== undefined) {
                    this.switchToReview();
                } else {
                    this.nextObjectGrid = this.playbackHistory[this.nextLoopedIndex(this.currentStep)].objectGrid;
                    this.playbackState = PlaybackState.retracting;
                    this.nextUpdateTime = timestamp + timeToNextUpdate;
                }
            } else {
                this.nextGameState = this.step(this.staticGrid, this.gameState);
                let hash = this.hashState(this.nextGameState);
                if (this.stateLookupTable[hash] !== undefined) {
                    // If the next state is one we've seen already, then create the loop.
                    // We do this check BEFORE the turn limit check, effectively giving the user a bonus turn past the end if it would result in a loop.
                    // Otherwise, if the turn limit was say 8, that would only allow loops of 7 or smaller to be found.
                    this.loopStart = this.stateLookupTable[hash];
                    this.nextGameState = this.playbackHistory[this.loopStart].state;
                    this.playbackState = PlaybackState.retracting;
                    this.nextUpdateTime = timestamp + GAME_MOVEMENT_DURATION;

                    let chargeCycle = !this.leaving; // Don't trigger a win if the player hit the home button
                    for (let i = this.loopStart; i < this.playbackHistory.length; i++) {
                        if (!this.playbackHistory[i].state.charged) {
                            chargeCycle = false;
                            break;
                        }
                    }
                    if (chargeCycle) {
                        let solved = defaults.get("solved");
                        solved[this.currentLevel] = true;
                        defaults.set("solved", solved);

                        this.haltReason = HaltReason.charging;
                        this.pauseButton.enabled = false;
                        this.restartButton.enabled = false;
                        let duration = GAME_VICTORY_ANIMATION_STATE_DURATIONS[VictoryAnimationState.speedup];
                        this.victoryAnimationState = VictoryAnimationState.speedup;
                        this.victoryAnimationStateChangeTime = timestamp + duration;
                        this.victoryChargeLoops = 0;
                        musicPlayer.setPlaybackRateOfCurrentChannels(1.1, duration / 1000);
                    } else {
                        this.haltReason = HaltReason.inconsistent;
                    }
                } else {
                    if (this.currentStep == GAME_TURN_LIMIT - 1) {
                        // Now we can finally do the turn limit check
                        this.haltReason = HaltReason.noCycle;
                        this.switchToReview();
                    } else if (this.nextGameState.error !== undefined) {
                        // And then an error check on the next state
                        this.playbackHistory[this.currentStep].errorInfo = this.nextGameState;
                        this.haltReason = this.nextGameState.error;
                        this.switchToReview();
                    } else {
                        // Everything's good! Keep scoochin'
                        this.playbackState = PlaybackState.retracting;
                        this.nextUpdateTime = timestamp + GAME_MOVEMENT_DURATION;
                    }
                }
            }
        }
    }

    // When reviewing, change state to what the user selected
    if (this.stateSelectTarget !== undefined) {
        if (this.stateSelectTarget > this.currentStep) {
            this.switchToStep(this.currentStep + 1);
        } else {
            this.switchToStep(this.currentStep - 1);
        }
        if (this.stateSelectTarget == this.currentStep) {
            this.stateSelectTarget = undefined;
        }
    }

    // Flash the eyes
    if (this.eyesFlashToggleTime !== undefined && timestamp >= this.eyesFlashToggleTime) {
        if (this.victoryAnimationState === VictoryAnimationState.pauseAfterCooldown && !this.eyesFlashing) {
            this.eyesFlashToggleTime = undefined;
        } else {
            this.eyesFlashToggleTime = timestamp + GAME_EYES_FLASH_INTERVAL;
            this.eyesFlashing = !this.eyesFlashing;
        }
    }

    // Do the ejac zoom
    mainContext.save();
    if (this.victoryAnimationState >= VictoryAnimationState.zoom) {
        zoomed = true;
        let progress = 1;
        if (this.victoryAnimationState === VictoryAnimationState.zoom) {
            progress = victoryAnimationStateProgress;
        }
        let scale = 1 + (GAME_ZOOM_SCALE - 1) * progress;
        let y = this.robotTop + ((this.robotTop - gameHeight / 2 - 60) / 3) * (progress);

        mainContext.imageSmoothingEnabled = false;
        mainContext.translate(275, y);
        mainContext.scale(scale, scale);
        mainContext.translate(-275, -y);
        mainContext.drawImage(imagePreloader.getImage("examinationroom"), 0, 0);
    }

    // Do the ejac shake
    let shakeAmplitude = 0;
    if (this.victoryAnimationState < VictoryAnimationState.zoom && this.atMaxSpeed) {
        shakeAmplitude = 0.5;
    } else if (this.victoryAnimationState === VictoryAnimationState.zoom) {
        shakeAmplitude = 0.5 + victoryAnimationStateProgress * (GAME_SHAKE_MAX_AMPLITUDE - 0.5);
    } else if (this.victoryAnimationState === VictoryAnimationState.pauseAfterZoom) {
        shakeAmplitude = GAME_SHAKE_MAX_AMPLITUDE;
    } else if (this.victoryAnimationState === VictoryAnimationState.cooldown) {
        shakeAmplitude = (1 - victoryAnimationStateProgress) * GAME_SHAKE_MAX_AMPLITUDE;
    }
    mainContext.translate(randomSign() * shakeAmplitude * Math.random(), randomSign() * shakeAmplitude * Math.random());

    // Translate for the item recoil/bounceback
    if (this.victoryAnimationState >= VictoryAnimationState.extendingItem) {
        if (this.victoryAnimationState === VictoryAnimationState.extendingItem) {
            this.itemRecoilY = 10 * victoryAnimationStateProgress;
        } else {
            let springValues = numericSpring(this.itemRecoilY, this.itemRecoilVelocity, 0, 0.1 * Math.PI, 0.01, dt);
            this.itemRecoilY = springValues.x;
            this.itemRecoilVelocity = springValues.v;
        }
        mainContext.translate(0, this.itemRecoilY);
    }

    // Draw da robot
    mainContext.drawImage(this.robotCanvas, this.robotLeft, this.robotTop);
    if (this.eyesFlashing) {
        mainContext.drawImage(this.eyesFlashCanvas, this.robotLeft + this.eyesXOffset, this.robotTop + this.eyesYOffset);
    }

    // This is all off screen after zooming
    if (this.victoryAnimationState === undefined || this.victoryAnimationState <= VictoryAnimationState.zoom) {
        // Draw the grid blocks
        mainContext.save();
        mainContext.translate(this.robotLeft + this.gridLeft, this.robotTop + this.gridTop);
        mainContext.beginPath();
        mainContext.rect(0, 0, this.gridWidth, this.gridHeight);
        mainContext.clip();

        for (let row = this.objectGrid.length - 1; row >= 0; row--) {
            for (let col = 0; col < this.objectGrid[0].length; col++) {
                let object = this.objectGrid[row][col];
                if (typeof object == "string") {
                    mainContext.drawImage(imagePreloader.getImage(object), col * GAME_TILE_SIZE, row * GAME_TILE_SIZE - GAME_TILE_INSET_SIZE);
                } else if (typeof object == "object" && (this.playbackState === PlaybackState.planning || this.playbackState === PlaybackState.reviewing || object.type === GridObject.body)) {
                    mainContext.save();
                    if (object.ghost) {
                        mainContext.globalAlpha = 0.5;
                    }

                    let highlighted = (object.index === this.highlightedCharger);
                    if (this.playbackState === PlaybackState.reviewing && this.playbackHistory[this.currentStep].errorInfo !== undefined) {
                        let errorInfo = this.playbackHistory[this.currentStep].errorInfo;
                        if (errorInfo.error === HaltReason.collision) {
                            highlighted = (object.index == errorInfo.a || object.index == errorInfo.b);
                        } else if (errorInfo.error === HaltReason.blockage) {
                            highlighted = (object.index == errorInfo.index);
                        }
                    }

                    mainContext.drawImage(imagePreloader.getImage(object.image + (highlighted ? "-highlighted" : "")), col * GAME_TILE_SIZE, row * GAME_TILE_SIZE - GAME_TILE_INSET_SIZE);
                    if (object.type === GridObject.head) {
                        mainContext.drawImage(imagePreloader.getImage("charger-eye" + object.direction + (object.index === this.highlightedCharger ? "-highlighted" : "")), col * GAME_TILE_SIZE, row * GAME_TILE_SIZE - GAME_TILE_INSET_SIZE);
                    }

                    mainContext.restore();
                } else if (typeof object == "object") {
                    let direction = object.direction;
                    let progress = (this.nextUpdateTime - timestamp) / GAME_MOVEMENT_DURATION;
                    progress = clamp(progress, 0, 1);
                    if (object.type === GridObject.head) {
                        progress = 1 - progress;
                    }

                    let fullSize = GAME_TILE_SIZE + GAME_TILE_INSET_SIZE;
                    let dx = col * GAME_TILE_SIZE;
                    let dy = row * GAME_TILE_SIZE - GAME_TILE_INSET_SIZE;
                    let dWidth = fullSize;
                    let dHeight = fullSize;

                    let sx = 0;
                    let sy = 0;
                    let sWidth = fullSize;
                    let sHeight = fullSize;

                    if ((this.playbackState === PlaybackState.extending && object.type === GridObject.head) || (this.playbackState === PlaybackState.retracting && object.type === GridObject.tail)) {
                        if (direction === Direction.right) {
                            dWidth = progress * fullSize;
                            sx = fullSize - progress * fullSize;
                            sWidth = progress * fullSize
                        } else if (direction === Direction.left) {
                            dx += fullSize - progress * fullSize;
                            dWidth = progress * fullSize;
                            sWidth = progress * fullSize;
                        } else if (direction === Direction.down) {
                            dHeight = progress * fullSize;
                            sy = fullSize - progress * fullSize;
                            sHeight = progress * fullSize;
                        } else {
                            dy += fullSize - progress * fullSize;
                            dHeight = progress * fullSize;
                            sHeight = progress * fullSize;
                        }
                    }

                    // drawImage will throw an IndexSizeError if a dimension is 0
                    // If a dimension is 0, then we're not drawing anything anyway
                    if (dWidth > 0 && dHeight > 0) {
                        mainContext.drawImage(imagePreloader.getImage(object.image), sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                        if (object.type === GridObject.head) {
                            mainContext.drawImage(imagePreloader.getImage("charger-eye" + direction), sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                        }
                    }
                } else if (object === GridObject.wall) {
                    mainContext.drawImage(this.blockCanvas, col * GAME_TILE_SIZE, row * GAME_TILE_SIZE - GAME_TILE_INSET_SIZE);
                }
            }
        }

        let cellsToHighlight = undefined, xOut = false;
        if (this.highlightedCell !== undefined) {
            cellsToHighlight = [this.highlightedCell];
        } else if (this.validNextCells !== undefined) {
            cellsToHighlight = this.validNextCells;
        } else if (this.playbackHistory !== undefined && this.playbackHistory[this.currentStep].errorInfo !== undefined && this.playbackHistory[this.currentStep].errorInfo.error === HaltReason.collision) {
            cellsToHighlight = [this.playbackHistory[this.currentStep].errorInfo.destination];
            xOut = true;
        }
        if (cellsToHighlight !== undefined) {
            mainContext.strokeStyle = "white";
            mainContext.lineWidth = 2;
            for (let i = 0; i < cellsToHighlight.length; i++) {
                let x = cellsToHighlight[i].col * GAME_TILE_SIZE + GAME_TILE_INSET_SIZE + 1;
                let y = cellsToHighlight[i].row * GAME_TILE_SIZE - GAME_TILE_INSET_SIZE + 1;
                mainContext.beginPath();
                mainContext.rect(x, y, GAME_TILE_SIZE - 1, GAME_TILE_SIZE - 1);
                if (xOut) {
                    mainContext.moveTo(x, y);
                    mainContext.lineTo(x + GAME_TILE_SIZE - 1, y + GAME_TILE_SIZE - 1);
                    mainContext.moveTo(x + GAME_TILE_SIZE - 1, y);
                    mainContext.lineTo(x, y + GAME_TILE_SIZE - 1);
                }
                mainContext.stroke();
            }
        }

        mainContext.restore(); // undoes the grid clip
    }

    // Draw the head item
    if (this.victoryAnimationState >= VictoryAnimationState.extendingItem) {
        let robotHead = defaults.get("robotHeads")[this.currentLevel];
        let headItemOffset = ((robotHead.head == 1) ? 5 : 8)

        if (this.victoryAnimationState === VictoryAnimationState.extendingItem) {
            let height = Math.max(1, 80 * victoryAnimationStateProgress);
            mainContext.drawImage(imagePreloader.getImage("headitem" + robotHead.headItem), 0, 0, 100, height, this.robotLeft + this.headLeft, this.robotTop - height + headItemOffset, 100, height)
        } else {
            mainContext.drawImage(imagePreloader.getImage("headitem" + robotHead.headItem), this.robotLeft + this.headLeft, this.robotTop - 80 + headItemOffset)
        }
    }
    mainContext.restore(); // undoes the ejac zoom & shake

    // Draw the tablet stuff
    tabletContext.fillStyle = "#9F0000"; // same color as the chargers
    tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

    // Buttons we'll draw unrotated
    for (let i = 0; i < this.buttons.length; i++) {
        if (this.buttons[i].image !== undefined && this.buttons[i].visible) {
            let button = this.buttons[i];
            tabletContext.drawImage(imagePreloader.getImage(button.image + (this.buttonIDs[this.activeButton] == button.id ? "-pressed" : "")), button.left, button.top);
        }
    }

    // Turn into screen space
    tabletContext.save();
    tabletContext.translate(tabletScreenWidth / 2, tabletScreenHeight / 2);
    tabletContext.rotate(-Math.PI / 2);
    tabletContext.translate(-tabletScreenWidth / 2, -tabletScreenHeight / 2);
    tabletContext.translate(87, -87);

    // Draw the remaining chargers
    if (this.playbackState === PlaybackState.planning) {
        tabletContext.fillStyle = "white";
        tabletContext.font = "40px serif";
        drawCenteredText(tabletContext, "Available:", GAME_TABLET_SLIVER_WIDTH / 2, 40, 40);

        let lengths = Object.keys(this.remainingChargers).sort();
        let row = 0, col = 0;
        tabletContext.strokeStyle = "black";
        tabletContextlineWidth = 2;
        for (let l = 0; l < lengths.length; l++) {
            let length = lengths[l];
            for (let i = 0; i < this.remainingChargers[length]; i++) {
                let x = 10 + col * 100, y = 85 + row * 100;
                tabletContext.fillStyle = (i == 0 && this.inProgressCharger !== undefined && this.inProgressCharger.tail.length + 1 == length) ? "#E00000" : "#570000";
                tabletContext.fillRect(x, y, 80, 80);
                tabletContext.strokeRect(x + 1, y + 1, 80 - 2, 80 - 2);

                tabletContext.fillStyle = "white";
                let cellRow = 0, cellCol = 0;
                for (let j = 0; j < length; j++) {
                    tabletContext.fillRect(x + 6 + cellCol * 24, y + 6 + cellRow * 24, 18, 18);
                    cellCol += 1;
                    if (cellCol == 3) {
                        cellCol = 0;
                        cellRow += 1;
                    }
                }

                col += 1;
                if (col == 3) {
                    col = 0;
                    row += 1;
                }
            }
        }
    }

    // Draw the history
    if (this.playbackState !== PlaybackState.planning && (this.victoryAnimationState === undefined || this.victoryAnimationState <= VictoryAnimationState.cooldown)) {
        tabletContext.fillStyle = "white";
        tabletContext.font = "40px serif";
        let statusMessage = "Running...";
        if (this.haltReason === HaltReason.collision) {
            statusMessage = "Collision";
        } else if (this.haltReason === HaltReason.blockage) {
            statusMessage = "Blocked charger";
        } else if (this.haltReason === HaltReason.inconsistent) {
            tabletContext.font = "35px serif";
            statusMessage = "Inconsistent charge";
        } else if (this.haltReason === HaltReason.noCycle) {
            tabletContext.font = "39px serif";
            statusMessage = "No cycle reached";
        } else if (this.haltReason === HaltReason.paused) {
            statusMessage = "Paused";
        } else if (this.haltReason === HaltReason.charging) {
            statusMessage = "Charging...";
        }
        drawText(tabletContext, statusMessage, 10, 40, 40);

        tabletContext.strokeStyle = "black";
        let row = 0, col = 0;
        for (let i = 0; i < this.playbackHistory.length; i++) {
            let x = 10 + col * 28, y = 90 + row * 50;

            tabletContext.lineWidth = 3;
            if (i === this.loopStart) {
                tabletContext.beginPath();
                tabletContext.moveTo(x + 4.5, y - 4.5);
                tabletContext.lineTo(x - 4.5, y - 4.5);
                tabletContext.lineTo(x - 4.5, y + 40 + 4.5);
                tabletContext.lineTo(x + 4.5, y + 40 + 4.5);
                tabletContext.stroke();
            } else if (this.loopStart !== undefined && i == this.playbackHistory.length - 1) {
                tabletContext.beginPath();
                tabletContext.moveTo(x + 18 - 4.5, y - 4.5);
                tabletContext.lineTo(x + 18 + 4.5, y - 4.5);
                tabletContext.lineTo(x + 18 + 4.5, y + 40 + 4.5);
                tabletContext.lineTo(x + 18 - 4.5, y + 40 + 4.5);
                tabletContext.stroke();
            }
            if (this.loopStart !== undefined && i >= this.loopStart) {
                tabletContext.lineWidth = 9;
                tabletContext.beginPath();
                tabletContext.moveTo(x - 5.5, y + 19.5);
                tabletContext.lineTo(x + 18 + 5.5, y + 19.5);
                tabletContext.stroke();
            }

            if (i === this.activeStateSelectButton) {
                tabletContext.fillStyle = "#808080";
            } else if (this.playbackHistory[i].errorInfo !== undefined) {
                tabletContext.fillStyle = "black";
            } else {
                tabletContext.fillStyle = "white";
            }
            tabletContext.fillRect(x, y, 18, 40);
            if (this.playbackHistory[i].state.charged) {
                tabletContext.drawImage(imagePreloader.getImage("chargedstate"), x + 3, y + 9);
            }

            if (i == this.currentStep) {
                tabletContext.lineWidth = 3;
                tabletContext.strokeRect(x - 1.5, y - 1.5, 21, 43);
            }

            col += 1;
            if (col == 10) {
                col = 0;
                row += 1;
            }
        }
    }

    // Fade things out during cooldown
    if (this.victoryAnimationState === VictoryAnimationState.cooldown) {
        tabletContext.fillStyle = "rgba(159, 0, 0, " + victoryAnimationStateProgress + ")";
        tabletContext.fillRect(0, 0, tabletScreenHeight, tabletScreenWidth);
    }

    // Draw the review box
    if (this.victoryAnimationState >= VictoryAnimationState.showingReviewBox) {
        let boxProgress = (this.victoryAnimationState === VictoryAnimationState.showingReviewBox) ? victoryAnimationStateProgress : 1;
        let x = 10, y = 40, width = (GAME_TABLET_SLIVER_WIDTH - 2 * 10) * boxProgress, height = (89 + 33 * this.reviewMessage.length) * boxProgress;
        tabletContext.fillStyle = "white";
        tabletContext.fillRect(x, y, width, height);
        tabletContext.strokeStyle = "black";
        tabletContext.lineWidth = 3;
        tabletContext.strokeRect(x + 1.5, y + 1.5, width - 3, height - 3);

        // Review contents
        if (this.victoryAnimationState >= VictoryAnimationState.fadingReviewContents) {
            // Round rect & head
            tabletContext.lineWidth = 3;
            tabletContext.fillStyle = defaults.get("robotHeads")[this.currentLevel].reviewColor;
            tabletContext.beginPath();
            tabletContext.moveTo(x + 60, y + 10);
            tabletContext.arc(x + 60, y + 20, 10, -Math.PI / 2, 0);
            tabletContext.lineTo(x + 70, y + 60);
            tabletContext.arc(x + 60, y + 60, 10, 0, Math.PI / 2);
            tabletContext.lineTo(x + 20, y + 70);
            tabletContext.arc(x + 20, y + 60, 10, Math.PI / 2, Math.PI);
            tabletContext.lineTo(x + 10, y + 20);
            tabletContext.arc(x + 20, y + 20, 10, Math.PI, 3 * Math.PI / 2);
            tabletContext.closePath();
            tabletContext.fill();
            tabletContext.stroke();
            tabletContext.drawImage(this.headCanvas, x + 40 - 25, y + 40 - 15, 50, 30);

            // Name and stars
            tabletContext.fillStyle = "black";
            tabletContext.font = "25px sans-serif";
            drawText(tabletContext, defaults.get("robots")[this.currentLevel].name, x + 80 + 1, y + 10, 25);
            tabletContext.drawImage(imagePreloader.getImage("5stars"), x + 80 + 4, y + 70 - 30);
        }

        // Message
        if (this.victoryAnimationState >= VictoryAnimationState.reviewing) {
            let charactersToShow = Math.floor((timestamp - this.reviewStartTime) / 100);
            let line = 0;
            while (charactersToShow > 0 && line < this.reviewMessage.length) {
                drawText(tabletContext, this.reviewMessage[line].slice(0, charactersToShow), x + 6, y + 83 + 33 * line, 25);
                charactersToShow -= this.reviewMessage[line].length + 5; // +5 for newline pause
                line += 1;
            }
        }

        // Fade
        if (this.victoryAnimationState === VictoryAnimationState.fadingReviewContents) {
            tabletContext.fillStyle = "rgba(255, 255, 255, " + (1 - victoryAnimationStateProgress) + ")";
            tabletContext.fillRect(x + 3, y + 3, width - 6, height - 6);
        }
    }

    tabletContext.restore();

    drawStatusBar(tabletContext, defaults.get("robots")[this.currentLevel].name, false);
}

GameSceneController.prototype.startTransitionIn = function(transitionType) {
    if (this.needsRefresh) {
        this.needsRefresh = false;
        let level = levels[this.currentLevel];
        let robot = defaults.get("robots")[this.currentLevel], robotHead = defaults.get("robotHeads")[this.currentLevel];

        // Calculate some dimensions
        let headTop = Math.round((robot.thickness + 10) / 2) - 60;

        this.gridLeft = GAME_ROBOT_GRID_PADDING + 2;
        this.gridWidth = level.level[0].length * GAME_TILE_SIZE;
        this.gridTop = GAME_ROBOT_GRID_PADDING + 2 + robot.thickness - headTop;
        this.gridHeight = level.level.length * GAME_TILE_SIZE;

        let torsoWidth = this.gridWidth + 4 + GAME_ROBOT_GRID_PADDING * 2;
        let torsoHeight = this.gridHeight + 4 + GAME_ROBOT_GRID_PADDING * 2;

        let robotWidth = torsoWidth + robot.thickness;
        this.robotLeft = Math.round(275 - robotWidth / 2);
        let robotHeight = torsoHeight + robot.thickness + 50 - headTop;
        this.robotTop = Math.round(gameHeight - robotHeight);
        this.headLeft = Math.round((torsoWidth + robot.thickness - 100) / 2);

        // Make the canvas for the robot bitmap
        this.robotCanvas = document.createElement("canvas");
        this.robotCanvas.width = robotWidth;
        this.robotCanvas.height = robotHeight;
        let robotCanvasContext = this.robotCanvas.getContext("2d");

        // Torso
        robotCanvasContext.drawImage(this.makePrismCanvas(torsoWidth, torsoHeight, robot.thickness, robot.color, robot.shadeColor), 0, -headTop);

        // Head
        // We draw the head and eyes into a saved canvas because we'll also use it in the post-victory review
        let headThings = [
            {image: "head" + robotHead.head + "-colormask", color: robot.color},
            {image: "head" + robotHead.head + "-shademask", color: robot.shadeColor},
            {image: "head" + robotHead.head + "-frame", color: "black"}
        ];
        this.headCanvas = this.makeCompositeCanvas(headThings, 100, 60);
        robotCanvasContext.drawImage(this.headCanvas, this.headLeft, 0);

        // Eyes
        let eyesThings = [
            {image: "eyes" + robotHead.eyes + "-colormask", color: robotHead.eyeColor},
            {image: "eyes" + robotHead.eyes + "-frame", color: "black"}
        ];
        this.eyesXOffset = this.headLeft + ((robotHead.head == 1) ? 22 : 17);
        this.eyesYOffset = (robotHead.head == 1) ? 13 : 17;
        this.headCanvas.getContext("2d").drawImage(this.makeCompositeCanvas(eyesThings, 56, 36), this.eyesXOffset - this.headLeft, this.eyesYOffset);
        this.eyesFlashCanvas = this.makeCompositeCanvas(
            [
                {image: "eyes" + robotHead.eyes + "-colormask", color: robotHead.eyeFlashColor},
                {image: "eyes" + robotHead.eyes + "-frame", color: "black"}
            ], 56, 36);
        robotCanvasContext.drawImage(this.headCanvas, this.headLeft, 0);

        // Empty out the playing grid
        robotCanvasContext.strokeStyle = "black";
        robotCanvasContext.lineWidth = 2;
        robotCanvasContext.strokeRect(this.gridLeft - 1, this.gridTop - 1, this.gridWidth + 2, this.gridHeight + 2);
        robotCanvasContext.fillStyle = robot.darkColor;
        robotCanvasContext.fillRect(this.gridLeft, this.gridTop, this.gridWidth, this.gridHeight);

        // Checkerboard
        robotCanvasContext.save();
        robotCanvasContext.beginPath();
        robotCanvasContext.rect(this.gridLeft, this.gridTop, this.gridWidth, this.gridHeight);
        robotCanvasContext.clip();
        robotCanvasContext.fillStyle = robot.shadeColor;
        for (let row = 0; row < level.level.length; row++) {
            for (let col = 0; col < level.level[0].length; col++) {
                if ((row + col) % 2) {
                    continue;
                }
                robotCanvasContext.fillRect(this.gridLeft + col * GAME_TILE_SIZE + GAME_TILE_INSET_SIZE, this.gridTop + row * GAME_TILE_SIZE - GAME_TILE_INSET_SIZE, GAME_TILE_SIZE, GAME_TILE_SIZE);
            }
        }
        robotCanvasContext.restore();

        // Draw the left & bottom insets of the playing grid
        robotCanvasContext.save();
        robotCanvasContext.translate(this.gridLeft, this.gridTop);
        robotCanvasContext.fillStyle = robot.shadeColor;
        robotCanvasContext.fillRect(0, 0, GAME_TILE_INSET_SIZE, this.gridHeight);
        robotCanvasContext.fillRect(0, this.gridHeight - GAME_TILE_INSET_SIZE, this.gridWidth, GAME_TILE_INSET_SIZE);
        robotCanvasContext.strokeStyle = "black";
        robotCanvasContext.lineWidth = 2;
        robotCanvasContext.beginPath();
        robotCanvasContext.moveTo(GAME_TILE_INSET_SIZE + 1, 0);
        robotCanvasContext.lineTo(GAME_TILE_INSET_SIZE + 1, this.gridHeight - GAME_TILE_INSET_SIZE - 1);
        robotCanvasContext.lineTo(this.gridWidth, this.gridHeight - GAME_TILE_INSET_SIZE - 1);
        robotCanvasContext.moveTo(GAME_TILE_INSET_SIZE + 1, this.gridHeight - GAME_TILE_INSET_SIZE - 1);
        robotCanvasContext.lineTo(0, this.gridHeight);
        robotCanvasContext.stroke();
        robotCanvasContext.restore();

        // Legs
        let legThings = [
            {image: "leg" + robot.legs + "-colormask", color: robot.color},
            {image: "leg" + robot.legs + "-shademask", color: robot.shadeColor},
            {image: "leg" + robot.legs + "-darkmask", color: robot.darkColor},
            {image: "leg" + robot.legs + "-frame", color: "black"}
        ];
        let leg = this.makeCompositeCanvas(legThings, 40, 50);
        robotCanvasContext.drawImage(leg, 20, robotHeight - 50);
        // Exactly symmetrical with the left leg would be robotWidth - robot.thickness - 40 - 20
        // However, that looks unbalanced because of perspective, so instead we'll position it conditionally based on how thick we are
        // 10 * (40 - robot.thickness) / 25 -> 10 for the lowest thickness (15), 0 for the highest (40)
        // That term is based on the range of possible thicknesses and should be changed if the range does
        let rightLegX = Math.round(robotWidth - robot.thickness - 40 - 10 * (40 - robot.thickness) / 25);
        robotCanvasContext.drawImage(leg, rightLegX, robotHeight - 50);

        // Load data
        this.initialGameState = defaults.get("solutions")[this.currentLevel];
        this.energizeButton.enabled = (this.initialGameState.chargers.length > 0);
        this.staticGrid = this.makeStaticGrid(level);
        this.remainingChargers = Object.assign({}, level.chargers);
        for (let i = 0; i < this.initialGameState.chargers.length; i++) {
            this.remainingChargers[this.initialGameState.chargers[i].tail.length + 1] -= 1;
        }

        // Make the block image
        this.blockCanvas = this.makePrismCanvas(GAME_TILE_SIZE, GAME_TILE_SIZE, GAME_TILE_INSET_SIZE, robot.color, robot.shadeColor);
    }

    this.playbackState = PlaybackState.planning;
    this.gameState = this.initialGameState;
    this.objectGrid = this.makeObjectGrid(this.staticGrid, this.gameState);
    this.nextGameState = undefined;
    this.nextObjectGrid = undefined;
    this.nextUpdateTime = undefined;

    this.plottingCharger = false;
    this.plottingChargerByDragging = false;
    this.hoveredCell = undefined;
    this.highlightedCell = undefined;
    this.inProgressCharger = undefined;
    this.currentPath = undefined;
    this.validNextCells = undefined;
    this.highlightedCharger = undefined;

    this.playbackHistory = undefined;
    this.stateLookupTable = undefined;
    this.currentStep = undefined;
    this.haltReason = undefined;
    this.loopStart = undefined;

    this.activeStateSelectButton = undefined;
    this.stateSelectTarget = undefined;

    this.atMaxSpeed = false;
    this.victoryAnimationState = undefined;
    this.victoryAnimationStateChangeTime = undefined;
    this.victoryChargeLoops = undefined;
    this.eyesFlashing = false;
    this.eyesFlashToggleTime = undefined;
    this.itemRecoilY = undefined;
    this.itemRecoilVelocity = undefined;
    this.reviewStartTime = undefined;
    this.reviewMessage = undefined;

    this.pauseButton.enabled = true;
    this.restartButton.enabled = true;

    // This scene uses the visibility property to draw buttons, which means they have to be attached.
    // So, we disable the buttons so you can't click them during transitions.
    disableButtons();
    addButton(this.energizeButton);
    this.energizeButton.enabled = (this.initialGameState.chargers.length > 0);

    musicPlayer.play("ingame" + levels[this.currentLevel].music + "-light");
}

GameSceneController.prototype.finishTransitionIn = function(transitionType) {
    // Do this to recalculate its bounds on the tablet after rotation's done lmao
    removeButton(this.energizeButton);
    addButton(this.energizeButton);
    enableButtons();

    this.plotButton.left = this.robotLeft + this.gridLeft + GAME_TILE_INSET_SIZE;
    this.plotButton.right = this.plotButton.left + this.gridWidth - 1;
    this.plotButton.top = this.robotTop + this.gridTop - GAME_TILE_INSET_SIZE;
    this.plotButton.bottom = this.plotButton.top + this.gridHeight - 1;
    addButton(this.plotButton);
}

GameSceneController.prototype.startTransitionOut = function(transitionType) {
    this.leaving = true;
    removeButtons(this.stateSelectButtons);
    disableButtons();

    musicPlayer.stop();
    soundPlayer.fadeOut("extend", 2 * transitionDuration / 1000);
}

GameSceneController.prototype.finishTransitionOut = function(transitionType) {
    this.leaving = false;
    removeButtons(this.buttons);
    enableButtons();
}


GameSceneController.prototype.loadLevel = function(level) {
    if (level !== this.currentLevel) {
        this.currentLevel = level;
        this.needsRefresh = true;
    }
}
