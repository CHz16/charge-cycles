// creditsscene.js
// Controller for the credits scene.

let CREDITS_TEXT = [
    {size: 48, x: -4, y: 0, robot: "\u286f\u28df\u28af\u285f\u2830\u286f", english: "Charge Cycles"},
    {x: -1, y: 50, robot: "\u286f\u2863\u2856\u2806\u2815\u28cf\u2823\u28aa\u2846\u28e2\u286f", english: "A Strawberry Jam 2 game"},
    {x: -1, y: 111, robot: "\u286f\u280c\u285e\u28a9\u286f", english: "Most stuff:"},
    {x: -1, y: 146, robot: "\u286f\u283d\u2896\u2890\u28ee\u2877\u288b\u2834\u286f", english: "CHz"},
    {x: -1, y: 207, robot: "\u286f\u2847\u28a3\u286f", english: "Music:"},
    {x: -1, y: 242, robot: "\u286f\u2832\u288f\u28c3\u28d9\u2850\u2856\u28f7\u286f", english: "Chimeratio"},
    {size: 24, y: 348, robot: "\u286f\u287e\u28d7\u2894\u28c3\u2820\u286f", english: "The abstract meteor icon:"},
    {size: 24, y: 377, robot: "\u286f\u28d6\u28d7\u2864\u28a2\u284b\u2845\u285e\u286f", english: "Aldric Rodríguez (the Noun Project)"},
    {size: 24, y: 421, robot: "\u286f\u2851\u2822\u284b\u28fb\u286f", english: "The modem noises:"},
    {size: 24, y: 450, robot: "\u286f\u2879\u2810\u2866\u28f0\u28bc\u28ac\u28bb\u286f", english: "Blue Neon (Freesound)"}
    // 434
];

function CreditsSceneController() {
    for (let i = 0; i < CREDITS_TEXT.length; i++) {
        if (CREDITS_TEXT[i].size === undefined) {
            CREDITS_TEXT[i].size = 32;
        }
        if (CREDITS_TEXT[i].x === undefined) {
            CREDITS_TEXT[i].x = 0;
        }

        let angle = 6 * i / CREDITS_TEXT.length, sextantProgress = angle % 1;
        let r = 0, g = 0, b = 0;
        if (angle < 1) {
            r = 255;
            g = 255 * sextantProgress;
        } else if (angle < 2) {
            r = 255 * (1 - sextantProgress);
            g = 255;
        } else if (angle < 3) {
            g = 255;
            b = 255 * sextantProgress;
        } else if (angle < 4) {
            g = 255 * (1 - sextantProgress);
            b = 255;
        } else if (angle < 5) {
            b = 255;
            r = 255 * sextantProgress;
        } else {
            b = 255 * (1 - sextantProgress);
            r = 255;
        }
        CREDITS_TEXT[i].color = "#" + Math.round(r).toString(16).padStart(2, "0") + Math.round(g).toString(16).padStart(2, "0") + Math.round(b).toString(16).padStart(2, "0");
    }
}

CreditsSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    tabletContext.fillStyle = "black";
    tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

    tabletContext.save();
    tabletContext.translate(0, 40);
    for (let i = 0; i < CREDITS_TEXT.length; i++) {
        let line = CREDITS_TEXT[i];
        tabletContext.font = line.size + "px serif";
        tabletContext.fillStyle = line.color;
        drawText(tabletContext, line.robot, 10 + line.x, line.y, line.size);

        let metrics = tabletContext.measureText(line.english);
        drawText(tabletContext, line.english, tabletScreenWidth - 10 - metrics.width, line.y, line.size);
    }
    tabletContext.restore();

    drawStatusBar(tabletContext, "Credits", true);
}

CreditsSceneController.prototype.startTransitionIn = function(transitionType) {

}

CreditsSceneController.prototype.finishTransitionIn = function(transitionType) {

}

CreditsSceneController.prototype.startTransitionOut = function(transitionType) {

}

CreditsSceneController.prototype.finishTransitionOut = function(transitionType) {

}
