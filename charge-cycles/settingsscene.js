// settingsscene.js
// Controller for the settings scene.

let SETTINGS_DIALOG_FADE_DURATION = 150; // milliseconds

function SettingsSceneController() {
    this.setVolume = function(sliderIndex, x) {
        let slider = this.sliders[sliderIndex];
        let volume = (x - 5 - slider.left) / (slider.right - slider.left - 10);
        volume = clamp(volume, 0, 1);
        defaults.set(slider.defaultsKey, volume);
        slider.player.setVolume(volume);
    }

    this.addNormalButtons = function() {
        addButtons(this.sliders);
        addButtons(this.backgroundButtons);
        addButton(this.deleteDataButton);
        removeButton(this.backgroundButtons[defaults.get("background") - 1]);
    }

    this.removeNormalButtons = function() {
        removeButtons(this.sliders);
        removeButtons(this.backgroundButtons);
        removeButton(this.deleteDataButton);
    }

    this.rows = [
        {label: "Sound Volume", y: 100},
        {label: "Music Volume", y: 150},
        {label: "Background", y: 250},
        {label: "Delete Saved Data", y: 350}
    ];

    // We want to play the sound even if the user mouse ups outside of the slider, so we need to trap this too
    this.mouseUp = function(x, y) {
        if (this.activeSlider === 0) {
            soundPlayer.play("logo");
        }
        this.activeSlider = undefined;
    }
    this.activeSlider = undefined;
    this.sliders = [];
    for (let i = 0; i < 2; i++) {
        this.sliders.push(new Button(
            460, 105 + 50 * i, 213, 40,
            {
                cursor: "grab",
                cursorDown: "grabbing",
                mouseDown: function(x, y) { soundPlayer.stop("logo"); this.activeSlider = i; this.setVolume(i, x); }.bind(this),
                mouseDrag: function(x, y) { this.setVolume(i, x); }.bind(this),
                mouseUp: function(x, y) { this.activeSlider = undefined; if (i == 0) { soundPlayer.play("logo"); } }.bind(this)
            }
        ));
        this.sliders[i].defaultsKey = (i == 0) ? "soundVolume" : "musicVolume";
        this.sliders[i].player = (i == 0) ? soundPlayer : musicPlayer;
    }

    this.activeBackgroundButton = undefined;
    this.backgroundButtons = [];
    for (let i = 0; i < 3; i++) {
        this.backgroundButtons.push(new Button(
            460 + 80 * i, 255, 53, 40,
            {
                mouseDown: function(x, y) { this.activeBackgroundButton = i; }.bind(this),
                mouseUp: function(x, y) {
                    removeButton(this.backgroundButtons[i]);
                    addButton(this.backgroundButtons[defaults.get("background") - 1]);
                    defaults.set("background", i + 1);
                    this.activeBackgroundButton = undefined;
                }.bind(this),
                mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeBackgroundButton = i; } }.bind(this),
                mouseLeave: function(x, y, mouseIsDown) { this.activeBackgroundButton = undefined; }.bind(this)
            }
        ));
    }

    this.deleteDataButtonIsDown = undefined;
    this.deleteDataButton = new Button(
        0, 350, tabletScreenWidth, 50,
        {
            mouseDown: function(x, y) { this.deleteDataButtonIsDown = true; }.bind(this),
            mouseUp: function(x, y) {
                this.removeNormalButtons();
                this.deleteDialogFadeEnd = performance.now() + SETTINGS_DIALOG_FADE_DURATION;
                this.deleteDataButtonIsDown = false;
            }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.deleteDataButtonIsDown = true; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.deleteDataButtonIsDown = false; }.bind(this)
        }
    );

    this.showingDeleteDialog = undefined;
    this.deleteDialogFadeEnd = undefined;
    this.leaving = undefined;

    // We need to calculate the data dialog box size ahead of time to create the buttons
    // So might as well calculate all the bounds
    tabletCanvasContext.font = "30px sans-serif";
    let metrics = tabletCanvasContext.measureText("Are you sure you want to");
    this.deleteDataDialogWidth = metrics.width + 20;
    this.deleteDataDialogLeft = (tabletScreenWidth - this.deleteDataDialogWidth) / 2;
    this.deleteDataDialogHeight = 35 * 3 + 10 + 35 + 10 + 10;
    this.deleteDataDialogTop = (tabletScreenHeight - this.deleteDataDialogHeight) / 2;

    this.activeDeleteDataDialogButton = undefined;
    this.deleteDataDialogButtons = [];
    for (let i = 0; i < 2; i++) {
        this.deleteDataDialogButtons.push(new Button(
            this.deleteDataDialogLeft + i * this.deleteDataDialogWidth / 2, this.deleteDataDialogTop + this.deleteDataDialogHeight - 40, this.deleteDataDialogWidth / 2, 40,
            {
                mouseDown: function(x, y) { this.activeDeleteDataDialogButton = i; }.bind(this),
                mouseUp: function(x, y) {
                    if (i == 0) {
                        initializeGameData();
                        scenes["game"].loadLevel(undefined); // kill the back button on the menu if the player was in-game when they came here
                        // Reset back to the normal menu theme if we were playing the 100% menu theme
                        if (musicPlayer.currentSong() !== "menu-full") {
                            musicPlayer.play("menu-full");
                        }
                    }
                    removeButtons(this.deleteDataDialogButtons);
                    this.deleteDialogFadeEnd = performance.now() + SETTINGS_DIALOG_FADE_DURATION;
                    this.activeDeleteDataDialogButton = undefined;
                }.bind(this),
                mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeDeleteDataDialogButton = i; } }.bind(this),
                mouseLeave: function(x, y, mouseIsDown) { this.activeDeleteDataDialogButton = undefined; }.bind(this)
            }
        ));
        this.deleteDataDialogButtons[i].label = (i ? "No!" : "Delete");
        let labelMetrics = tabletCanvasContext.measureText(this.deleteDataDialogButtons[i].label);
        this.deleteDataDialogButtons[i].labelLeft = this.deleteDataDialogLeft + ((i * 2 + 1) * this.deleteDataDialogWidth / 2 - labelMetrics.width) / 2;
    }
}

SettingsSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    tabletContext.fillStyle = "#A0A0A0";
    tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

    // just the rows & labels
    tabletContext.strokeStyle = "black";
    tabletContext.lineWidth = 2;
    tabletContext.font = "30px sans-serif";
    for (let i = 0; i < this.rows.length; i++) {
        let row = this.rows[i];

        tabletContext.fillStyle = "#D0D0D0";
        tabletContext.fillRect(0, row.y, tabletScreenWidth, 50);
        tabletContext.strokeRect(0, row.y, tabletScreenWidth, 50);

        tabletContext.fillStyle = "black";
        drawText(tabletContext, row.label, 10, row.y + 11, 30);
    }

    // sound & music sliders
    tabletContext.strokeStyle = "red";
    tabletContext.lineWidth = 3;
    for (let i = 0; i < this.sliders.length; i++) {
        let slider = this.sliders[i];

        tabletContext.fillStyle = "black";
        tabletContext.beginPath();
        tabletContext.moveTo(slider.left, slider.bottom);
        tabletContext.lineTo(slider.right, slider.bottom);
        tabletContext.lineTo(slider.right, slider.top);
        tabletContext.lineTo(slider.left, slider.bottom);
        tabletContext.closePath();
        tabletContext.fill();

        tabletContext.fillStyle = "white";
        tabletContext.fillRect(slider.left + defaults.get(slider.defaultsKey) * (slider.right - slider.left - 10), slider.top - 0.5, 10, 40);
        tabletContext.strokeRect(slider.left + defaults.get(slider.defaultsKey) * (slider.right - slider.left - 10), slider.top - 0.5, 10, 40);
    }

    // background change buttons
    for (let i = 0; i < this.backgroundButtons.length; i++) {
        let button = this.backgroundButtons[i];
        tabletContext.drawImage(imagePreloader.getImage("background" + (i + 1)), button.left, button.top, 53, 40);

        if (defaults.get("background") == (i + 1)) {
            tabletContext.lineWidth = 3;
            tabletContext.strokeStyle = "red";
            tabletContext.strokeRect(button.left - .5, button.top - .5, 54, 41);
        } else if (this.activeBackgroundButton == i) {
            tabletContext.fillStyle = "rgba(0, 0, 0, 0.5)";
            tabletContext.fillRect(button.left, button.top, 53, 40);
        }
    }

    // delete button
    if (this.deleteDataButtonIsDown) {
        tabletContext.fillStyle = "rgba(0, 0, 0, 0.5)";
        tabletContext.fillRect(this.deleteDataButton.left, this.deleteDataButton.top, this.deleteDataButton.right - this.deleteDataButton.left + 1, this.deleteDataButton.bottom - this.deleteDataButton.top + 1);
    }

    // delete dialog
    if (timestamp >= this.deleteDialogFadeEnd) {
        // Only activate buttons after the fade if we're not transitioning out
        if (!this.leaving) {
            if (this.showingDeleteDialog) {
                this.addNormalButtons();
            } else {
                addButtons(this.deleteDataDialogButtons);
            }
        }

        this.showingDeleteDialog = !this.showingDeleteDialog;
        this.deleteDialogFadeEnd = undefined;
    }
    if (this.showingDeleteDialog || this.deleteDialogFadeEnd !== undefined) {
        let progress = 1;
        if (this.deleteDialogFadeEnd !== undefined) {
            progress = (this.deleteDialogFadeEnd - timestamp) / SETTINGS_DIALOG_FADE_DURATION;
            progress = clamp(progress, 0, 1); // may end up being < 0 because of a performance.now() setting of this.deleteDialogFadeEnd
            if (!this.showingDeleteDialog) {
                progress = 1 - progress;
            }
        }

        // bg
        tabletContext.save();
        tabletContext.globalAlpha = 0.5 * progress;
        tabletContext.fillStyle = "black";
        tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

        // box
        tabletContext.globalAlpha = progress;
        tabletContext.fillStyle = "white";
        tabletContext.fillRect(this.deleteDataDialogLeft, this.deleteDataDialogTop, this.deleteDataDialogWidth, this.deleteDataDialogHeight);
        tabletContext.strokeStyle = "black";
        tabletContext.lineWidth = 2;
        tabletContext.strokeRect(this.deleteDataDialogLeft, this.deleteDataDialogTop, this.deleteDataDialogWidth, this.deleteDataDialogHeight);

        // text
        tabletContext.fillStyle = "black";
        drawText(tabletContext, "Are you sure you want to", this.deleteDataDialogLeft + 10, this.deleteDataDialogTop + 10, 30);
        drawText(tabletContext, "delete your saved data?", this.deleteDataDialogLeft + 10, this.deleteDataDialogTop + 45, 30);
        drawText(tabletContext, "This can't be undone!", this.deleteDataDialogLeft + 10, this.deleteDataDialogTop + 80, 30);

        // buttons
        for (let i = 0; i < this.deleteDataDialogButtons.length; i++) {
            let button = this.deleteDataDialogButtons[i];
            tabletContext.strokeRect(button.left, button.top, button.right - button.left + 1, button.bottom - button.top + 1);
            tabletContext.fillStyle = "black";
            drawText(tabletContext, button.label, button.labelLeft, button.top + 6, 30);
            if (this.activeDeleteDataDialogButton === i) {
                tabletContext.fillStyle = "rgba(0, 0, 0, 0.5)";
                tabletContext.fillRect(button.left, button.top, button.right - button.left + 1, button.bottom - button.top + 1);
            }
        }
        tabletContext.restore();
    }

    drawStatusBar(tabletContext, "Settings", true);
}

SettingsSceneController.prototype.startTransitionIn = function(transitionType) {
    this.activeSlider = undefined;
    this.activeBackgroundButton = undefined;

    this.deleteDataButtonIsDown = false;
    this.showingDeleteDialog = false;
    this.deleteDialogFadeEnd = undefined;
    this.leaving = false;
}

SettingsSceneController.prototype.finishTransitionIn = function(transitionType) {
    this.addNormalButtons();
}

SettingsSceneController.prototype.startTransitionOut = function(transitionType) {
    this.leaving = true;
    this.removeNormalButtons();
    removeButtons(this.deleteDataDialogButtons);
}

SettingsSceneController.prototype.finishTransitionOut = function(transitionType) {

}
