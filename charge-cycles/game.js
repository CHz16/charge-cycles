// game.js
// Charge Cycles main game code.

//
// GAME PARAMETERS
//

let gameWidth = 800, gameHeight = 600;
let tabletScreenWidth = 696, tabletScreenHeight = 522;
let tabletInsetWidth = 52, tabletInsetHeight = 39;

let batteryLife = 60 * 60 * 1000, batteryChargeTime = 30 * 60 * 1000; // milliseconds

let transitionDuration = 500; // milliseconds


//
// GLOBALS
//

let audioContext;
let soundPlayer, musicPlayer;

let canvas, canvasContext;
let tabletCanvas, tabletCanvasContext, offscreenTabletCanvas, offscreenTabletCanvasContext;
let imagePreloader;

let scenes = [], currentScene = "loading";

let lastUpdateTime;
let currentTransitionType, transitionStartTime, deferredDestination;
let tabletOrientation = 0; // spectrum between 0 = horiz full screen, 1 = vert on right

let canvasX, canvasY;
let mouseDownInCanvas = false, mouseWithinButton = undefined, mouseDownInButton = undefined, savedHoveredCursor = undefined;
let buttonsAreActive = true, nextButtonID = 0, attachedButtons = [];

let backButton, backButtonIsPressed = false;

let defaults = new JSUserDefaults("chargecycles_", {
    musicVolume: 0.75,
    soundVolume: 1,
    timePowered: 0
});


//
// INITIALIZATION
//

window.addEventListener("load", init);
function init() {
    let canvasThings = makeScaledCanvas(gameWidth, gameHeight);
    canvas = canvasThings.canvas;
    canvasContext = canvasThings.context;

    // Replace HTML canvas with new canvas and set its display size
    document.body.replaceChild(canvas, document.getElementById("canvas"));
    canvas.style.width = gameWidth + "px";
    canvas.style.height = gameHeight + "px";

    let tabletCanvasThings = makeScaledCanvas(tabletScreenWidth, tabletScreenHeight);
    tabletCanvas = tabletCanvasThings.canvas;
    tabletCanvasContext = tabletCanvasThings.context;

    let offscreenTabletCanvasThings = makeScaledCanvas(tabletScreenWidth, tabletScreenHeight);
    offscreenTabletCanvas = offscreenTabletCanvasThings.canvas;
    offscreenTabletCanvasContext = offscreenTabletCanvasThings.context;

    canvas.onmouseup = function(evt) {
        canvas.onmouseup = undefined;
        init2();
    }
    drawErrorMessage("Click to play");
}

function init2() {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    audioContext.resume(); // Safari 12 autoplay policy pls

    // Load just the images that the loading scene will need immediately
    // This is a temporary image preloader and will be replaced by the "real" one in the loading scene
    let allCallback = function(preloader, imagesHandled, errors) {
        if (imagesHandled == preloader.urls.length) {
            init3();
        }
    };
    let errorCallback = function(preloader, imagesHandled, index) {
        drawErrorMessage("Something went wrong loading " + preloader.keys[index]);
        return true;
    };
    imagePreloader = new ImagePreloader(["strawberry-lofi.png", "tabletframe.png", "examinationroom.png"], {base: "images/", keys: ["strawberry-lofi", "tabletframe", "examinationroom"], allCallback: allCallback, errorCallback: errorCallback});

    // Assign a random background if one hasn't been set already. We do it this way to permanently save it; if we did this in the defaults object then you could get a new random one next page load if you don't save it.
    if (defaults.get("background") === null) {
        defaults.set("background", randomIntInRange(1, 3));
    }

    // Make the back button. We'll handle it, not the scenes.
    // This button "lives in tablet space" so that the orientation changes will apply to it too.
    backButton = new Button(
        749 - 52, 276 - 39, 47, 47,
        {
            mouseDown: function(x, y) { backButtonIsPressed = true; },
            mouseUp: function(x, y) { backButtonIsPressed = false; startTransition("menu", (currentScene == "game") ? Transition.rotateOut : Transition.tabletFade); },
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { backButtonIsPressed = true; } },
            mouseLeave: function(x, y, mouseIsDown) { backButtonIsPressed = false; }
        }
    );
}

function init3() {
    if (defaults.get("solved") === null) {
        initializeGameData();
    } else {
        migrateGameData();
    }

    // We need to keep track of the upper left position of the canvas for mouse position calculations
    findCanvasPosition();
    document.body.addEventListener("resize", findCanvasPosition);

    // Trap dem mice
    window.addEventListener("mousedown", mouseDownHandler);
    window.addEventListener("mouseup", mouseUpHandler);
    window.addEventListener("mousemove", mouseMoveHandler);

    // Construct the loading scene, which will load images/sounds/music and construct every other scene
    scenes["loading"] = new LoadingSceneController();
    scenes[currentScene].startTransitionIn(Transition.immediate);
    scenes[currentScene].finishTransitionIn(Transition.immediate);

    // Kick off the draw loop
    window.requestAnimationFrame(draw);
}

function initializeGameData() {
    defaults.set("solved", Array(levels.length).fill(false))

    let ids = [], robots = [], robotHeads = [], solutions = [];
    for (let i = 0; i < levels.length; i++) {
        ids.push(levels[i].id);
        solutions.push({chargers: []});

        let r = randomRobot();
        robots.push(r.robot);
        robotHeads.push(r.head);
    }
    defaults.set("ids", ids);
    defaults.set("robots", robots);
    defaults.set("robotHeads", robotHeads);
    defaults.set("solutions", solutions);
}

// Reorders the saved completion and robot data, if the order of puzzles has changed since last play.
// This is done by saving the puzzle IDs and comparing that order with the new order.
function migrateGameData() {
    let savedIDs = defaults.get("ids"), newIDs = [];
    let needsMigration = (savedIDs.length != levels.length);
    for (let i = 0; i < levels.length; i++) {
        if (savedIDs[i] != levels[i].id) {
            needsMigration = true;
        }
        newIDs.push(levels[i].id);
    }

    if (!needsMigration) {
        return;
    }

    let puzzleLocations = {};
    for (let i = 0; i < savedIDs.length; i++) {
        puzzleLocations[savedIDs[i]] = i;
    }

    let savedSolved = defaults.get("solved"), newSolved = [];
    let savedSolutions = defaults.get("solutions"), newSolutions = [];
    let savedRobots = defaults.get("robots"), newRobots = [];
    let savedRobotHeads = defaults.get("robotHeads"), newRobotHeads = [];
    for (let i = 0; i < levels.length; i++) {
        if (puzzleLocations[levels[i].id] !== undefined) {
            let j = puzzleLocations[levels[i].id];
            newSolved.push(savedSolved[j]);
            newSolutions.push(savedSolutions[j]);
            newRobots.push(savedRobots[j]);
            newRobotHeads.push(savedRobotHeads[j]);
        } else {
            newSolved.push(false);
            newSolutions.push({chargers: []});
            let newRobot = randomRobot();
            newRobots.push(newRobot.robot);
            newRobotHeads.push(newRobot.head);
        }
    }

    defaults.set("ids", newIDs);
    defaults.set("solved", newSolved);
    defaults.set("solutions", newSolutions);
    defaults.set("robots", newRobots);
    defaults.set("robotHeads", newRobotHeads);
}


//
// DRAWING
//

// Creates a canvas and 2D drawing context for that canvas that are set up for HiDPI drawing.
// Be careful not to undo the base scale transform on the canvas context with .resetTransform() or anything
// Also be careful to, when you're compositing canvases with .drawImage(), specify the "base" width and height of the canvas and not the scaled size
function makeScaledCanvas(width, height) {
    let scale = window.devicePixelRatio || 1;

    let canvas = document.createElement("canvas");
    canvas.width = width * scale;
    canvas.height = height * scale;

    let context = canvas.getContext("2d");
    context.scale(scale, scale);
    context.imageSmoothingEnabled = false;

    return {canvas: canvas, context: context};
}

// Draws text with an upper left of (x, y)
// Could also do this with context.textBaseline = "top", but that renders inconsistently between FF and Safari/Chrome
function drawText(context, text, x, y, fontSize) {
    context.fillText(text, Math.round(x), Math.round(y + 3 * fontSize / 4));
}

function drawCenteredText(context, text, x, y, fontSize) {
    let metrics = context.measureText(text);
    drawText(context, text, x - metrics.width / 2, y, fontSize);
}

function drawErrorMessage(text) {
    canvasContext.font = "30px sans-serif";
    canvasContext.fillStyle = "black";
    drawCenteredText(canvasContext, text, gameWidth / 2, 300, 30);
}

function draw(timestamp) {
    if (lastUpdateTime === undefined) {
        lastUpdateTime = timestamp;
    }
    let dt = timestamp - lastUpdateTime;
    lastUpdateTime = timestamp;

    // Update the battery
    let newTimePowered = (defaults.get("timePowered") + dt) % (batteryLife + batteryChargeTime);
    defaults.set("timePowered", newTimePowered);

    // Update the current transition, if we're in the middle of one.
    if (currentTransitionType !== undefined) {
        if (timestamp >= (transitionStartTime + transitionDuration)) {
            finishTransition();
        }
    }

    // Clear the canvases
    canvasContext.drawImage(imagePreloader.getImage("examinationroom"), 0, 0);
    tabletCanvasContext.fillStyle = "white";
    tabletCanvasContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

    // Send draw to the current scene and, if applicable, scene that's transitioning in
    // This will do the main canvas drawing and construct the tablet screen without drawing it
    if (currentTransitionType === undefined) {
        scenes[currentScene].draw(timestamp, dt, canvasContext, tabletCanvasContext);
    } else {
        let transitionProgress = (timestamp - transitionStartTime) / transitionDuration;
        if (currentTransitionType === Transition.rotateIn) {
            tabletOrientation = transitionProgress;
        } else if (currentTransitionType === Transition.rotateOut) {
            tabletOrientation = 1 - transitionProgress;
        }

        offscreenTabletCanvasContext.fillStyle = "white";
        offscreenTabletCanvasContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);
        scenes[currentScene].draw(timestamp, dt, canvasContext, offscreenTabletCanvasContext);
        tabletCanvasContext.globalAlpha = 1 - transitionProgress;
        tabletCanvasContext.drawImage(offscreenTabletCanvas, 0, 0, tabletScreenWidth, tabletScreenHeight);

        scenes[deferredDestination].draw(timestamp, dt, canvasContext, offscreenTabletCanvasContext);
        tabletCanvasContext.globalAlpha = 1 - tabletCanvasContext.globalAlpha;
        tabletCanvasContext.drawImage(offscreenTabletCanvas, 0, 0, tabletScreenWidth, tabletScreenHeight);

        tabletCanvasContext.globalAlpha = 1;
    }

    // Draw the tablet screen and frame in the correct orientation
    if (tabletOrientation == 0) {
        canvasContext.drawImage(tabletCanvas, tabletInsetWidth, tabletInsetHeight, tabletScreenWidth, tabletScreenHeight);
        canvasContext.drawImage(imagePreloader.getImage(backButtonIsPressed ? "tabletframe-pressed" : "tabletframe"), 0, 0);
    } else {
        canvasContext.save();
        canvasContext.translate(375 * tabletOrientation, 0);
        canvasContext.translate(400, 300);
        canvasContext.scale(1 - 0.25 * tabletOrientation, 1 - 0.25 * tabletOrientation);
        canvasContext.rotate(tabletOrientation * Math.PI / 2);
        canvasContext.translate(-400, -300);
        canvasContext.drawImage(tabletCanvas, tabletInsetWidth, tabletInsetHeight, tabletScreenWidth, tabletScreenHeight);
        canvasContext.drawImage(imagePreloader.getImage(backButtonIsPressed ? "tabletframe-pressed" : "tabletframe"), 0, 0);
        canvasContext.restore();
    }

    // Request another draw
    window.requestAnimationFrame(draw);
}

function drawStatusBar(context, title, isHorizontal) {
    context.save();
    let width = tabletScreenWidth;
    if (!isHorizontal) {
        context.translate(tabletScreenWidth / 2, tabletScreenHeight / 2);
        context.rotate(-Math.PI / 2);
        context.translate(-tabletScreenWidth / 2, -tabletScreenHeight / 2);
        context.translate(87, -87)

        width = tabletScreenHeight;
    }

    context.fillStyle = "white";
    context.fillRect(0, 0, width, 30);

    context.fillStyle = "black";
    context.font = "20px sans-serif";
    drawCenteredText(context, title, width / 2, 8, 20);

    let time = new Date().toLocaleTimeString();
    let timeMetrics = context.measureText(time);
    drawText(context, time, width - timeMetrics.width - 6, 8, 20);

    let timePowered = defaults.get("timePowered");
    context.fillStyle = "#33AA00";
    if (timePowered <= batteryLife) {
        context.fillRect(8, 8, 36 * (batteryLife - timePowered) / batteryLife, 16);
        context.drawImage(imagePreloader.getImage("battery"), 6, 6);
    } else {
        context.fillRect(8, 8, 36 * (timePowered - batteryLife) / batteryChargeTime, 16);
        context.drawImage(imagePreloader.getImage("battery-charging"), 6, 6);
    }

    context.restore();
}


//
// INPUT
//

// Used for mouse position calculations
function findCanvasPosition() {
    canvasX = canvas.offsetLeft;
    canvasY = canvas.offsetTop;

    var obj = canvas;
    while (obj = obj.offsetParent) {
        canvasX += obj.offsetLeft;
        canvasY += obj.offsetTop;
    }
}

function mouseDownHandler(event) {
    let x = event.pageX - canvasX, y = event.pageY - canvasY;

    // If the user doesn't mouse down inside the canvas, then just ignore the click
    if (x < 0 || x >= gameWidth || y < 0 || y >= gameHeight) {
        return;
    }
    mouseDownInCanvas = true;

    if (buttonsAreActive) {
        // Check if the user moused down in a button and send mouseDown to it if so
        for (let i = 0; i < attachedButtons.length; i++) {
            if (attachedButtons[i].enabled && attachedButtons[i].pointIsWithin(x, y)) {
                mouseDownInButton = attachedButtons[i];
                if (mouseDownInButton.cursorDown !== undefined) {
                    canvas.style.cursor = mouseDownInButton.cursorDown;
                }

                let buttonX = (mouseDownInButton.onTabletScreen ? x - tabletInsetWidth : x);
                let buttonY = (mouseDownInButton.onTabletScreen ? y - tabletInsetHeight : y);
                mouseDownInButton.mouseDown(buttonX, buttonY);
                return;
            }
        }
    }

    // If the user didn't mouse down in a button, then just send a raw mouseDown event to the current scene
    if (scenes[currentScene].mouseDown !== undefined) {
        scenes[currentScene].mouseDown(x, y);
    }
}

function mouseUpHandler(event) {
    let x = event.pageX - canvasX, y = event.pageY - canvasY;

    // Only track the mouse up if the user started mousing down inside the canvas.
    // The mouse up location may be outside the canvas
    if (!mouseDownInCanvas) {
        return;
    }
    mouseDownInCanvas = false;

    if (buttonsAreActive) {
        // If the user started mouse downing in a button and mouse upped in that same button, then send mouseUp to it
        if (mouseDownInButton !== undefined) {
            for (let i = 0; i < attachedButtons.length; i++) {
                if (attachedButtons[i].enabled && mouseDownInButton.id == attachedButtons[i].id && mouseDownInButton.pointIsWithin(x, y)) {
                    canvas.style.cursor = mouseDownInButton.cursor;
                    let buttonX = (mouseDownInButton.onTabletScreen ? x - tabletInsetWidth : x);
                    let buttonY = (mouseDownInButton.onTabletScreen ? y - tabletInsetHeight : y);
                    mouseDownInButton.mouseUp(buttonX, buttonY);
                    mouseDownInButton = undefined;
                    savedHoveredCursor = undefined;
                    return;
                }
            }
            mouseDownInButton = undefined;
        }
    }

    // Restore the cursor to what it should be, in case it isn't
    // If it isn't, then that should mean what happened is we mouse downed on a button with cursorDown set, but mouse upped somewhere other than that button. If we moved the mouse over a different button before letting go, then mouseMoveHandler will have saved the cursor of that button.
    canvas.style.cursor = savedHoveredCursor || "auto";
    savedHoveredCursor = undefined;

     // If the user didn't mouse up in a button, then just send a raw mouseUp event to the current scene
    if (scenes[currentScene].mouseUp !== undefined) {
        scenes[currentScene].mouseUp(x, y);
    }
}

function mouseMoveHandler(event) {
    let x = event.pageX - canvasX, y = event.pageY - canvasY;

    if (buttonsAreActive) {
        // See if the mouse is inside a button
        let hoveredButton;
        for (let i = 0; i < attachedButtons.length; i++) {
            if (attachedButtons[i].enabled && attachedButtons[i].pointIsWithin(x, y)) {
                hoveredButton = attachedButtons[i];
                break;
            }
        }

        savedHoveredCursor = (hoveredButton !== undefined) ? hoveredButton.cursor : undefined;

        // Handle mouseEnter/mouseLeave on buttons
        if (mouseWithinButton !== undefined && (hoveredButton === undefined || hoveredButton.id != mouseWithinButton.id)) {
            let buttonX = (mouseWithinButton.onTabletScreen ? x - tabletInsetWidth : x);
            let buttonY = (mouseWithinButton.onTabletScreen ? y - tabletInsetHeight : y);
            mouseWithinButton.mouseLeave(buttonX, buttonY, mouseDownInButton !== undefined && mouseDownInButton.id == mouseWithinButton.id);
        }
        if (hoveredButton !== undefined && (mouseWithinButton === undefined || hoveredButton.id != mouseWithinButton.id)) {
            let buttonX = (hoveredButton.onTabletScreen ? x - tabletInsetWidth : x);
            let buttonY = (hoveredButton.onTabletScreen ? y - tabletInsetHeight : y);
            hoveredButton.mouseEnter(buttonX, buttonY, mouseDownInButton !== undefined && mouseDownInButton.id == hoveredButton.id);
        }
        mouseWithinButton = hoveredButton;

        // If the mouse is in a button, then change the cursor to that button's cursor
        // We don't care if they started dragging from outside the canvas or anything
        if (mouseDownInButton && mouseDownInButton.cursorDown !== undefined) {
            canvas.style.cursor = mouseDownInButton.cursorDown;
        } else {
            canvas.style.cursor = (mouseWithinButton !== undefined) ? mouseWithinButton.cursor : "auto";
        }

        // If the user started mouse downing in a button, then send mouseDrag to that button, no matter where the cursor is
        // Otherwise, if the user moved the mouse over a button, then send it mouseMove
        if (mouseDownInButton !== undefined) {
            let buttonX = (mouseDownInButton.onTabletScreen ? x - tabletInsetWidth : x);
            let buttonY = (mouseDownInButton.onTabletScreen ? y - tabletInsetHeight : y);
            mouseDownInButton.mouseDrag(buttonX, buttonY);
            return;
        } else if (mouseWithinButton !== undefined) {
            let buttonX = (mouseWithinButton.onTabletScreen ? x - tabletInsetWidth : x);
            let buttonY = (mouseWithinButton.onTabletScreen ? y - tabletInsetHeight : y);
            mouseWithinButton.mouseMove(buttonX, buttonY);
            return;
        }
    }

    // If the user is moving the mouse outside the canvas, then don't give the current scene the event
    if (x < 0 || x >= gameWidth || y < 0 || y >= gameHeight) {
        return;
    }

    // If the user isn't moving after mouse downing in a button, then send a raw mouseMove event to the current scene
    if (scenes[currentScene].mouseMove !== undefined) {
        scenes[currentScene].mouseMove(x, y);
    }
}


//
// BUTTONS
//

function Button(x, y, width, height, options) {
    this.id = nextButtonID;
    nextButtonID += 1;

    this.visible = false;
    this.enabled = true;

    this.left = x;
    this.right = x + width - 1;
    this.top = y;
    this.bottom = y + height - 1;

    options = options || { };
    this.onTabletScreen = (options.onTabletScreen !== undefined) ? options.onTabletScreen : true;
    this.mouseDown = options.mouseDown || function(x, y) { };
    this.mouseUp = options.mouseUp || function(x, y) { };
    this.mouseMove = options.mouseMove || function(x, y) { };
    this.mouseDrag = options.mouseDrag || function(x, y) { };
    this.mouseEnter = options.mouseEnter || function(x, y, mouseIsDown) { };
    this.mouseLeave = options.mouseLeave || function(x, y, mouseIsDown) { };
    this.cursor = options.cursor || "pointer";
    this.cursorDown = options.cursorDown;
}

Button.prototype.pointIsWithin = function(x, y) {
    let left = this.realLeft + (this.onTabletScreen ? tabletInsetWidth : 0);
    let right = this.realRight + (this.onTabletScreen ? tabletInsetWidth : 0);
    let top = this.realTop + (this.onTabletScreen ? tabletInsetHeight : 0);
    let bottom = this.realBottom + (this.onTabletScreen ? tabletInsetHeight : 0);
    return (x >= this.realLeft && x <= this.realRight && y >= this.realTop && y <= this.realBottom);
}

Button.prototype.calculateBoundsInOrientation = function() {
    let left = this.left + (this.onTabletScreen ? tabletInsetWidth : 0);
    let right = this.right + (this.onTabletScreen ? tabletInsetWidth : 0);
    let top = this.top + (this.onTabletScreen ? tabletInsetHeight : 0);
    let bottom = this.bottom + (this.onTabletScreen ? tabletInsetHeight : 0);

    if (!this.onTabletScreen || tabletOrientation == 0) {
        this.realLeft = left;
        this.realTop = top;
        this.realRight = right;
        this.realBottom = bottom;
    } else {
        // Applies the matrix transformations done when the screen is rotated
        this.realLeft = -0.75 * bottom + 1000;
        this.realTop = 0.75 * left;
        this.realRight = -0.75 * top + 1000;
        this.realBottom = 0.75 * right;
    }
}

function addButton(button) {
    for (let i = 0; i < attachedButtons.length; i++) {
        if (button.id == attachedButtons[i].id) {
            return;
        }
    }
    button.calculateBoundsInOrientation();
    attachedButtons.push(button);
    button.visible = true;
}

function addButtons(buttons) {
    for (let i = 0; i < buttons.length; i++) {
        addButton(buttons[i]);
    }
}

function removeButton(button) {
    for (let i = 0; i < attachedButtons.length; i++) {
        if (button.id == attachedButtons[i].id) {
            attachedButtons.splice(i, 1);
            button.visible = false;
            return;
        }
    }
}

function removeButtons(buttons) {
    for (let i = 0; i < buttons.length; i++) {
        removeButton(buttons[i]);
    }
}

function enableButtons() {
    buttonsAreActive = true;
}

function disableButtons() {
    buttonsAreActive = false;
    mouseWithinButton = undefined;
    mouseDownInButton = undefined;
    canvas.style.cursor = "auto";
}


//
// SCENE TRANSITIONS
//

let Transition = {immediate: 0, tabletFade: 1, rotateIn: 2, rotateOut: 3};

function startTransition(destination, transitionType) {
    scenes[currentScene].startTransitionOut(transitionType);
    scenes[destination].startTransitionIn(transitionType);

    currentTransitionType = transitionType;
    transitionStartTime = performance.now();
    deferredDestination = destination;
    if (currentTransitionType === Transition.immediate) {
        finishTransition();
    }

    disableButtons();
    removeButton(backButton);
}

function finishTransition() {
    if (currentTransitionType === Transition.rotateIn) {
        tabletOrientation = 1;
    } else if (currentTransitionType === Transition.rotateOut) {
        tabletOrientation = 0;
    }

    scenes[currentScene].finishTransitionOut(currentTransitionType);
    currentScene = deferredDestination;
    scenes[currentScene].finishTransitionIn(currentTransitionType);

    currentTransitionType = undefined;
    transitionStartTime = undefined;
    deferredDestination = undefined;

    enableButtons();
    if (currentScene != "menu") {
        addButton(backButton);
    }
}


