// musicplayer.js
// Object for playback of music.

let MUSIC_PLAYER_FADE_DURATION = 2; // seconds

function MusicPlayer(musicData, context) {
    this.channelPositionInfo = function(channel) {
        let songData = this.musicData[channel.key];
        let currentPosition = this.context.currentTime - channel.startTime;
        if (songData.loopStart !== undefined && currentPosition > songData.loopStart) {
            currentPosition = (currentPosition - songData.loopStart) % (songData.buffer.duration - songData.loopStart) + songData.loopStart;
        } else {
            currentPosition = currentPosition % songData.buffer.duration;
        }
        return {currentPosition: currentPosition, duration: songData.buffer.duration};
    }


    this.musicData = musicData;
    this.musicChannels = [];
    this.context = context;

    this.masterGainNode = this.context.createGain();
    this.masterGainNode.gain.setValueAtTime(1, 0);
    this.masterGainNode.connect(this.context.destination);

    // The music player needs an AnalyserNode for the waveform visualization, so we pass the music into that before sending it to the master gain for overall volume control.
    // However, an AnalyserNode can't combine multiple inputs, so we need to consolidate all the music channels into another GainNode first before passing it to the analyser.
    this.analyserNode = this.context.createAnalyser();
    this.analyserNode.fftSize = 2048;
    this.analyserNode.connect(this.masterGainNode);
    this.consolidatorNode = this.context.createGain();
    this.consolidatorNode.connect(this.analyserNode);

    this.cullTimeout = undefined;
}

MusicPlayer.prototype.play = function(key, args) {
    if (args === undefined) {
        args = {};
    }
    let loop = (args.loop !== undefined) ? args.loop : true;
    let sync = (args.sync !== undefined) ? args.sync : false;

    this.stop();

    let sourceNode = this.context.createBufferSource();
    sourceNode.buffer = this.musicData[key].buffer;
    if (loop) {
        sourceNode.loop = true;
    }
    if (this.musicData[key].loopStart !== undefined) {
        sourceNode.loopStart = this.musicData[key].loopStart;
    }

    let gainNode = this.context.createGain();
    gainNode.gain.setValueAtTime(0, 0);

    this.musicChannels.push({sourceNode: sourceNode, gainNode: gainNode, startTime: this.context.currentTime, key: key, fading: false});

    sourceNode.connect(gainNode);
    gainNode.connect(this.consolidatorNode);
    if (!sync || this.musicChannels.length == 0) {
        sourceNode.start();
    } else {
        // Sync with the previous channel, which is second from the end (since the last one is the channel we just made)
        let syncChannel = this.musicChannels[this.musicChannels.length - 2], syncData = this.musicData[syncChannel.key];
        this.musicChannels[this.musicChannels.length - 1].startTime = syncChannel.startTime;
        let currentPosition = this.channelPositionInfo(syncChannel).currentPosition;
        sourceNode.start(this.context.currentTime + delay, currentPosition);
    }
    gainNode.gain.setTargetAtTime(1, 0, MUSIC_PLAYER_FADE_DURATION / 4);
}

MusicPlayer.prototype.stop = function(fadeDuration) {
    if (this.musicChannels.length == 0) {
        return;
    }

    if (this.cullTimeout !== undefined) {
        window.clearTimeout(this.cullTimeout);
    }

    let time = (fadeDuration === undefined) ? MUSIC_PLAYER_FADE_DURATION : fadeDuration;
    for (let i = 0; i < this.musicChannels.length; i++) {
        this.musicChannels[i].gainNode.gain.setTargetAtTime(0, 0, time / 4);
        this.musicChannels[i].fading = true;
    }

    window.setTimeout(function() {
        for (let i = 0; i < this.musicChannels.length - 1; i++) {
            let channel = this.musicChannels[i];
            channel.sourceNode.stop();
            channel.sourceNode.disconnect();
            channel.gainNode.disconnect();
        }
        this.musicChannels = [this.musicChannels[this.musicChannels.length - 1]];
        this.cullTimeout = undefined;
    }.bind(this), time * 1000);
}

MusicPlayer.prototype.setVolume = function(volume) {
    this.masterGainNode.gain.setValueAtTime(volume, 0);
}

MusicPlayer.prototype.setPlaybackRateOfCurrentChannels = function(rate, time) {
    for (let i = 0; i < this.musicChannels.length; i++) {
        this.musicChannels[i].sourceNode.playbackRate.setTargetAtTime(rate, 0, time / 4);
    }
}

MusicPlayer.prototype.currentSong = function() {
    let lastChannel = this.musicChannels[this.musicChannels.length - 1];
    if (lastChannel === undefined || lastChannel.fading) {
        return null;
    } else {
        return lastChannel.key;
    }
}

MusicPlayer.prototype.currentSongPositionInfo = function() {
    let lastChannel = this.musicChannels[this.musicChannels.length - 1];
    if (lastChannel === undefined || lastChannel.fading) {
        return null;
    } else {
        return this.channelPositionInfo(lastChannel);
    }
}
