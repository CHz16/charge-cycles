// gamescene-simulations.js
// Simulation-related extensions to GameSceneController.

let Direction = {up: 0, down: 1, left: 2, right: 3};

let movementDeltas = [];
movementDeltas[Direction.up] = {row: -1, col: 0};
movementDeltas[Direction.down] = {row: 1, col: 0};
movementDeltas[Direction.left] = {row: 0, col: -1};
movementDeltas[Direction.right] = {row: 0, col: 1};

let turnDirections = [];
turnDirections[Direction.up] = Direction.right;
turnDirections[Direction.down] = Direction.left;
turnDirections[Direction.left] = Direction.up;
turnDirections[Direction.right] = Direction.down;

let doubleTurnDirections = [];
doubleTurnDirections[Direction.up] = Direction.left;
doubleTurnDirections[Direction.down] = Direction.right;
doubleTurnDirections[Direction.left] = Direction.down;
doubleTurnDirections[Direction.right] = Direction.up;

let reverseDirections = [];
reverseDirections[Direction.up] = Direction.down;
reverseDirections[Direction.down] = Direction.up;
reverseDirections[Direction.left] = Direction.right;
reverseDirections[Direction.right] = Direction.left;

let GridObject = {empty: 0, wall: 1, head: 2, tail: 3, body: 4};
let HaltReason = {collision: 0, blockage: 1, inconsistent: 2, noCycle: 3, paused: 4, charging: 5}; // lmao this enum tracks like three semantically different things now oops


GameSceneController.prototype.copyGrid = function(grid) {
    let newGrid = [];
    for (let i = 0; i < grid.length; i++) {
        newGrid.push(grid[i].slice(0));
    }
    return newGrid;
}

GameSceneController.prototype.makeStaticGrid = function(level) {
    let grid = [];
    // Make the whole grid now because we may need to plot objects ahead of where we are
    for (let i = 0; i < level.level.length; i++) {
        grid.push(Array(level.level[0].length).fill(GridObject.empty));
    }

    for (let row = 0; row < level.level.length; row++) {
        for (let col = 0; col < level.level[0].length; col++) {
            if (level.level[row][col] == "#") {
                grid[row][col] = GridObject.wall;
            } else if (level.level[row][col] == "B") {
                grid[row][col] = "batteryblock-ul";
                grid[row][col + 1] = "batteryblock-ur";
                grid[row + 1][col] = "batteryblock-ll";
                grid[row + 1][col + 1] = "batteryblock-lr";
            }
            // pass on . (floor) and - (battery dummy blocks)
        }
    }
    return grid;
}

GameSceneController.prototype.makeObjectGrid = function(staticGrid, state) {
    let grid = this.copyGrid(staticGrid);

    // Drop the chargers down
    for (let i = 0; i < state.chargers.length; i++) {
        let charger = state.chargers[i];
        let row = charger.head.row, col = charger.head.col;

        // Length 0 only happens at the start of drawing and we'll special case it
        if (charger.tail.length == 0) {
            grid[row][col] = {image: "charger-0010", type: GridObject.head, direction: Direction.right, ghost: charger.ghost, index: i};
            continue;
        }

        for (let j = 0; j < charger.tail.length; j++) {
            let code = ["0", "0", "0", "0"];
            code[charger.tail[j]] = "1";
            if (j > 0) {
                code[reverseDirections[charger.tail[j - 1]]] = "1";
            }

            grid[row][col] = {
                image: "charger-" + code.join(""),
                type: (j == 0) ? GridObject.head : GridObject.body,
                direction: reverseDirections[charger.tail[0]],
                ghost: charger.ghost,
                index: i
            };

            row += movementDeltas[charger.tail[j]].row;
            col += movementDeltas[charger.tail[j]].col;
        }

        let image;
        if (charger.tail[charger.tail.length - 1] === Direction.up) {
            image = "charger-0100";
        } else if (charger.tail[charger.tail.length - 1] === Direction.down) {
            image = "charger-1000";
        } else if (charger.tail[charger.tail.length - 1] === Direction.left) {
            image = "charger-0001";
        } else {
            image = "charger-0010";
        }
        grid[row][col] = {image: image, type: GridObject.tail, direction: charger.tail[charger.tail.length - 1], ghost: charger.ghost, index: i};
    }

    // Check the charging status of all the batteries
    state.charged = true;
    for (let row = 0; row < grid.length; row++) {
        cellscan: for (let col = 0; col < grid[0].length; col++) {
            if (grid[row][col] != "batteryblock-ul") {
                continue;
            }

            let probes = [
                {row: row - 1, col: col},
                {row: row - 1, col: col + 1},
                {row: row, col: col + 2},
                {row: row + 1, col: col + 2},
                {row: row + 2, col: col + 1},
                {row: row + 2, col: col},
                {row: row + 1, col: col - 1},
                {row: row, col: col - 1}
            ];
            for (let i = 0; i < probes.length; i++) {
                let probe = probes[i];
                if (probe.row < 0 || probe.row >= grid.length || probe.col < 0 || probe.col >= grid[0].length) {
                    continue;
                }
                if (typeof grid[probe.row][probe.col] == "object") {
                    grid[row][col] = "batteryblock-ul-charged";
                    grid[row][col + 1] = "batteryblock-ur-charged";
                    grid[row + 1][col] = "batteryblock-ll-charged";
                    grid[row + 1][col + 1] = "batteryblock-lr-charged";
                    continue cellscan;
                }
            }
            state.charged = false;
        }
    }

    return grid;
}

GameSceneController.prototype.step = function(staticGrid, state) {
    // Trim the tails off the chargers
    let newState = {chargers: []}, directions = [];
    for (let i = 0; i < state.chargers.length; i++) {
        let charger = state.chargers[i];
        directions.push(reverseDirections[charger.tail[0]]);
        newState.chargers.push({
            head: {row: charger.head.row, col: charger.head.col},
            tail: charger.tail.slice(0, -1)
        });
    }

    // Check which directions the chargers are going and find what squares they want to end up on
    let interimObjectGrid = this.makeObjectGrid(staticGrid, newState);
    let destinations = [];
    outer: for (let i = 0; i < newState.chargers.length; i++) {
        let charger = newState.chargers[i];

        let trialDirections = [directions[i], turnDirections[directions[i]], doubleTurnDirections[directions[i]]];
        for (let j = 0; j < trialDirections.length; j++) {
            let destination = {
                row: charger.head.row + movementDeltas[trialDirections[j]].row,
                col: charger.head.col + movementDeltas[trialDirections[j]].col
            };
            if (destination.row < 0 || destination.row >= staticGrid.length || destination.col < 0 || destination.col >= staticGrid[0].length) {
                continue;
            }
            if (interimObjectGrid[destination.row][destination.col] === GridObject.empty) {
                directions[i] = trialDirections[j];
                destinations.push(destination);
                continue outer;
            }
        }

        // Sim error: blocked charger
        return {error: HaltReason.blockage, index: i};
    }

    // Check if there are any collisions
    for (let i = 0; i < destinations.length - 1; i++) {
        for (let j = i + 1; j < destinations.length; j++) {
            if (destinations[i].row == destinations[j].row && destinations[i].col == destinations[j].col) {
                // Sim error: collision
                return {error: HaltReason.collision, a: i, b: j, destination: destinations[i]};
            }
        }
    }

    // Actually move the chargers
    for (let i = 0; i < newState.chargers.length; i++) {
        let charger = newState.chargers[i];
        charger.head.row = destinations[i].row;
        charger.head.col = destinations[i].col;
        charger.tail = [reverseDirections[directions[i]]].concat(charger.tail);
    }

    return newState;
}

GameSceneController.prototype.hashState = function(state) {
    if (state.chargers === undefined) {
        return undefined;
    }

    let indices = [];
    for (let i = 0; i < state.chargers.length; i++) {
        indices.push(i);
    }

    indices.sort(function(a, b) {
        let chargerA = state.chargers[a], chargerB = state.chargers[b];
        if (chargerA.head.row < chargerB.head.row) {
            return -1;
        } else if (chargerA.head.row > chargerB.head.row) {
            return 1;
        }

        if (chargerA.head.col < chargerB.head.col) {
            return -1;
        } else if (chargerA.head.col == chargerB.head.col) {
            return 0;
        } else {
            return 1;
        }
    });

    let hash = "";
    for (let i = 0; i < indices.length; i++) {
        let charger = state.chargers[indices[i]];
        hash += charger.head.row + "," + charger.head.col + "," + charger.tail.join("") + "|";
    }
    return hash;
}
