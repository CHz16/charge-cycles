// loadingscene.js
// Controller for the loading scene.

let LOADING_PROGRESS_BAR_MAX_SPEED = 1 / 1000; // % of the bar per millisecond, 1/1000 will fill the bar in 1 second
let LOADING_PROGRESS_BAR_FADEOUT_DURATION = 250; // milliseconds
let LOADING_SEGMENT_STRETCH = 5; // pixels
let LOADING_SEGMENT_DELAY = 25; // milliseconds
let LOADING_SEGMENT_TRANSITION_LENGTH = 50; // milliseconds
let LOADING_LOGO_HOLD = 1500; // milliseconds

function LoadingSceneController() {
    this.newImagePreloader = undefined;
    this.mediaLoaded = undefined, this.mediaCount = undefined;
    this.progress = undefined;
    this.logoStartTime = undefined
    this.transitionStartTime = undefined, this.transitionStarted = undefined;
    this.errorThing = undefined;
}

LoadingSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    tabletContext.fillStyle = "black";
    tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

    if (this.errorThing !== undefined) {
        let text = "Error loading " + this.errorThing;
        tabletContext.font = "30px sans-serif";
        tabletContext.fillStyle = "white";
        drawCenteredText(tabletContext, text, tabletScreenWidth / 2, 250, 30);
        return;
    }

    if (this.logoStartTime === undefined) {
        // Move the progress bar forward according to the speed limit
        this.progress = Math.min(this.progress + dt * LOADING_PROGRESS_BAR_MAX_SPEED, this.mediaLoaded / this.mediaCount);

        // strawb
        tabletContext.drawImage(imagePreloader.getImage("strawberry-lofi"), tabletScreenWidth / 2 - 50, 175);

        // progress bar
        tabletContext.strokeStyle = "white";
        tabletContext.lineWidth = 2;
        tabletContext.strokeRect(tabletScreenWidth / 2 - 51, 299, 102, 6);
        tabletContext.fillStyle = "white";
        tabletContext.fillRect(tabletScreenWidth / 2 - 50, 300, this.progress * 100, 4);

        // If we've loaded everything, construct the rest of the scenes and start moving
        if (this.progress == 1) {
            scenes["menu"] = new MenuSceneController();
            scenes["settings"] = new SettingsSceneController();
            scenes["musicplayer"] = new MusicPlayerSceneController();
            scenes["clients"] = new ClientsSceneController();
            scenes["game"] = new GameSceneController();
            scenes["manual"] = new ManualSceneController();
            scenes["credits"] = new CreditsSceneController();
            if (typeof TestSceneController !== "undefined") {
                scenes["test"] = new TestSceneController();
            }

            this.logoStartTime = timestamp;
            soundPlayer.play("logo");
        }
    } else if (this.transitionStartTime === undefined) {
        let x = tabletScreenWidth / 2 - 50;
        let y = 175;

        for (let row = 9; row >= 0; row--) {
            for (let col = 0; col < 10; col++) {
                let offset = (9 - row) + col;
                let segmentStartTime = this.logoStartTime + offset * LOADING_SEGMENT_DELAY;
                let segmentProgress = (timestamp - segmentStartTime) / LOADING_SEGMENT_TRANSITION_LENGTH;

                if (segmentProgress <= 0) {
                    tabletContext.drawImage(imagePreloader.getImage("strawberry-lofi"), col * 10, row * 10, 10, 10, x + col * 10, y + row * 10, 10, 10);
                } else if (segmentProgress <= 0.5) {
                    let stretch = 2 * LOADING_SEGMENT_STRETCH * segmentProgress;
                    tabletContext.drawImage(imagePreloader.getImage("strawberry-lofi"), col * 10, row * 10, 10, 10, x + col * 10 - stretch / 2, y + row * 10 - stretch / 2, 10 + stretch, 10 + stretch);
                } else if (segmentProgress < 1) {
                    let stretch = 2 * LOADING_SEGMENT_STRETCH * (1 - segmentProgress);
                    tabletContext.drawImage(imagePreloader.getImage("strawberry"), col * 10, row * 10, 10, 10, x + col * 10 - stretch / 2, y + row * 10 - stretch / 2, 10 + stretch, 10 + stretch);
                } else {
                    tabletContext.drawImage(imagePreloader.getImage("strawberry"), col * 10, row * 10, 10, 10, x + col * 10, y + row * 10, 10, 10);
                }
            }
        }

        tabletContext.fillStyle = "rgba(255, 255, 255, " + (1 - (timestamp - this.logoStartTime) / LOADING_PROGRESS_BAR_FADEOUT_DURATION) + ")";
        tabletContext.fillRect(tabletScreenWidth / 2 - 52, 298, this.progress * 104, 8);

        let finalSegmentEndTime = this.logoStartTime + 18 * LOADING_SEGMENT_DELAY + LOADING_SEGMENT_TRANSITION_LENGTH;
        if (timestamp >= finalSegmentEndTime) {
            this.transitionStartTime = finalSegmentEndTime + LOADING_LOGO_HOLD;
        }
    } else {
        tabletContext.drawImage(imagePreloader.getImage("strawberry"), tabletScreenWidth / 2 - 50, 175);
        if (!this.transitionStarted && timestamp >= this.transitionStartTime) {
            this.transitionStarted = true;
            startTransition("menu", Transition.tabletFade);
        }
    }
}

LoadingSceneController.prototype.startTransitionIn = function(transitionType) {
    this.mediaLoaded = 0, this.mediaCount = imageKeys.length + sounds.length + music.length;
    this.progress = 0;
    this.logoStartTime = undefined;
    this.transitionStartTime = undefined;
    this.transitionStarted = false;
    this.errorThing = undefined;

    let musicLoadCallback = function(preloader, soundsHandled, index) {
        if (this.errorThing !== undefined) {
            return true;
        }
        this.mediaLoaded += 1;
        if (soundsHandled == preloader.sounds.length) {
            let musicData = {};
            for (let i = 0; i < preloader.sounds.length; i++) {
                musicData[music[i].key] = {buffer: preloader.soundBuffers[music[i].key]};
                if (music[i].loopStart !== undefined) {
                    musicData[music[i].key].loopStart = music[i].loopStart;
                }
            }
            musicPlayer = new MusicPlayer(musicData, audioContext);
            musicPlayer.setVolume(defaults.get("musicVolume"));
        }
    }.bind(this);
    let musicErrorCallback = function(preloader, soundsHandled, index) {
        this.errorThing = "music: " + preloader.sounds[index].key;
        return true;
    }.bind(this);
    let musicPreloader = new SoundPreloader(music, audioContext, {base: "music/", loadCallback: musicLoadCallback, errorCallback: musicErrorCallback});

    let soundLoadCallback = function(preloader, soundsHandled, index) {
        if (this.errorThing !== undefined) {
            return true;
        }
        this.mediaLoaded += 1;
        if (soundsHandled == preloader.sounds.length) {
            soundPlayer = new SoundPlayer(preloader.soundBuffers, audioContext);
            soundPlayer.setVolume(defaults.get("soundVolume"));
        }
    }.bind(this);
    let soundErrorCallback = function(preloader, soundsHandled, index) {
        this.errorThing = "sound: " + preloader.sounds[index].key;
        return true;
    }.bind(this);
    let soundPreloader = new SoundPreloader(sounds, audioContext, {base: "sounds/", loadCallback: soundLoadCallback, errorCallback: soundErrorCallback});

    let imageLoadCallback = function(preloader, imagesHandled, index) {
        if (this.errorThing !== undefined) {
            return true;
        }
        this.mediaLoaded += 1;
        if (imagesHandled == preloader.urls.length) {
            imagePreloader = this.newImagePreloader;
        }
    }.bind(this);
    let imageErrorCallback = function(preloader, imagesHandled, index) {
        this.errorThing = "image: " + preloader.keys[index];
        return true;
    }.bind(this);
    this.newImagePreloader = new ImagePreloader(imageFilenames, {base: "images/", keys: imageKeys, loadCallback: imageLoadCallback, errorCallback: imageErrorCallback});
}

LoadingSceneController.prototype.finishTransitionIn = function(transitionType) {

}

LoadingSceneController.prototype.startTransitionOut = function(transitionType) {

}

LoadingSceneController.prototype.finishTransitionOut = function(transitionType) {

}
