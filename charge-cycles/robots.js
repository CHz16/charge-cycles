// robots.js
// Data stuff for making robots.

function randomRobot() {
    let colors = randomRobotColors();
    return {
        robot: {
            name: randomRobotLanguage(7),
            color: colors.color,
            shadeColor: colors.shadeColor,
            darkColor: colors.darkColor,
            thickness: randomIntInRange(15, 40),
            legs: randomIntInRange(1, 3)
        },
        head: {
            head: randomIntInRange(1, 3),
            eyes: randomIntInRange(1, 6),
            eyeColor: randomEyeColor(),
            eyeFlashColor: randomEyeColor(),
            headItem: randomIntInRange(1, 4),
            reviewColor: randomEyeColor()
        }
    };
}

let ROBOT_LANGUAGE_STRING_DELIMITER = 0x286F; // ⡯
function randomRobotLanguage(length) {
    let name = String.fromCharCode(ROBOT_LANGUAGE_STRING_DELIMITER); // delimiter at start of string
    for (let j = 0; j < length; j++) {
        let c;
        do {
            c = randomIntInRange(0x2800, 0x28FF); // Braille block minus delimiter
        } while (c == ROBOT_LANGUAGE_STRING_DELIMITER)
        name += String.fromCharCode(c);
    }
    name += String.fromCharCode(ROBOT_LANGUAGE_STRING_DELIMITER); // delimiter at end of string
    return name;
}

function randomRobotColors() {
    let pivot = randomIntInRange(0x40, 0xA0);
    let r = pivot + randomIntInRange(-0x10, 0x10);
    let g = pivot + randomIntInRange(-0x10, 0x10);
    let b = pivot + randomIntInRange(-0x10, 0x10);
    return {
        color: "#" + r.toString(16) + g.toString(16) + b.toString(16),
        shadeColor: "#" + Math.round(r * 0.75).toString(16) + Math.round(g * 0.75).toString(16) + Math.round(b * 0.75).toString(16),
        darkColor: "#" + Math.round(r * 0.5).toString(16) + Math.round(g * 0.5).toString(16) + Math.round(b * 0.5).toString(16)
    };
}

function randomEyeColor() {
    let r = randomIntInRange(0x80, 0xFF);
    let g = randomIntInRange(0x80, 0xFF);
    let b = randomIntInRange(0x80, 0xFF);
    return "#" + r.toString(16) + g.toString(16) + b.toString(16);
}
