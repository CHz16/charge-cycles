// testscene.js
// Controller for the test scene.

function TestSceneController() {

}

TestSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    tabletContext.fillStyle = "gray";
    tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);
}

TestSceneController.prototype.startTransitionIn = function(transitionType) {

}

TestSceneController.prototype.finishTransitionIn = function(transitionType) {

}

TestSceneController.prototype.startTransitionOut = function(transitionType) {

}

TestSceneController.prototype.finishTransitionOut = function(transitionType) {

}
