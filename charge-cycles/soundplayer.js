// soundplayer.js
// Object for simple playback of sound effects.

function SoundPlayer(soundBuffers, context) {
    this.soundBuffers = soundBuffers;
    this.soundChannels = new Object();
    this.context = context;

    this.masterGain = this.context.createGain();
    this.masterGain.gain.setValueAtTime(1, 0);
    this.masterGain.connect(this.context.destination);
}

SoundPlayer.prototype.play = function(key, args) {
    args = args || {};
    delay = args.delay || 0;
    offset = args.offset || 0;
    loop = args.loop || false;

    if (key in this.soundChannels) {
        this.stop(key);
    }

    let sourceNode = this.context.createBufferSource(), gainNode = this.context.createGain();
    this.soundChannels[key] = {sourceNode: sourceNode, gainNode: gainNode};

    sourceNode.buffer = this.soundBuffers[key];
    if (loop) {
        sourceNode.loop = true;
    }
    sourceNode.onended = function() { this.stop(key); }.bind(this);
    sourceNode.connect(gainNode);
    gainNode.connect(this.masterGain);
    sourceNode.start(this.context.currentTime + delay, offset);
}

SoundPlayer.prototype.stop = function(key) {
    if (key in this.soundChannels) {
        this.soundChannels[key].sourceNode.onended = undefined;
        this.soundChannels[key].sourceNode.stop();
        this.soundChannels[key].gainNode.disconnect();
        delete this.soundChannels[key];
    }
}

SoundPlayer.prototype.stopAllSounds = function() {
    for (key in this.soundChannels) {
        this.stop(key);
    }
}

SoundPlayer.prototype.fadeOut = function(key, fadeDuration) {
    if (key in this.soundChannels) {
        this.soundChannels[key].gainNode.gain.setTargetAtTime(0, 0, fadeDuration / 4);
    }
}

SoundPlayer.prototype.setVolume = function(volume) {
    this.masterGain.gain.setValueAtTime(volume, 0);
}
