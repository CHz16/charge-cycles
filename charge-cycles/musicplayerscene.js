// musicplayerscene.js
// Controller for the music player scene.

let MUSIC_PLAYER_SONG_INFO = [
    {name: "Welcome to Ulka!", id: "menu", togglable: false},
    {name: "Electrostatic Discharge", id: "ingame1", togglable: true},
    {name: "Mechanism Turned On", id: "ingame2", togglable: true},
    {name: "Transformer Robot Sing Wheels On The Bus", id: "ingame3", togglable: true},
    {name: "No One Cares About the Letter L", id: "ingame4", togglable: true},
    {name: "Charger Insertion", id: "ingame5", togglable: true},
    {name: "Alt.", id: "menu2", togglable: false}
];

let MUSIC_PLAYER_TOGGLE_DURATION = 100, MUSIC_PLAYER_TOGGLE_ENABLE_DURATION = 100; // milliseconds
let MUSIC_PLAYER_WAVEFORM_HEIGHT = 326;

function MusicPlayerSceneController() {
    this.switchToSong = function(delta, sync = false) {
        this.currentSong = (this.currentSong + delta + MUSIC_PLAYER_SONG_INFO.length) % MUSIC_PLAYER_SONG_INFO.length;
        let songInfo = MUSIC_PLAYER_SONG_INFO[this.currentSong];
        if (!songInfo.togglable || this.toggled) {
            musicPlayer.play(songInfo.id + "-full", {sync: sync});
        } else {
            musicPlayer.play(songInfo.id + "-light", {sync: sync});
        }

        if (this.toggleEnabled != songInfo.togglable) {
            this.toggleEnableAnimationEnd = performance.now() + MUSIC_PLAYER_TOGGLE_ENABLE_DURATION;
            this.toggleEnabled = !this.toggleEnabled;
            if (!this.toggleEnabled) {
                removeButton(this.toggleButton);
            }
        }
    }


    this.leaving = undefined;
    this.bufferLength = undefined, this.dataArray = undefined;
    this.currentSong = undefined;
    this.toggleEnabled = undefined, this.toggled = undefined;

    this.previousSongButtonIsDown = undefined;
    this.previousSongButton = new Button(
        0, tabletScreenHeight - 83, 83, 83,
        {
            mouseDown: function(x, y) { this.previousSongButtonIsDown = true; }.bind(this),
            mouseUp: function(x, y) {
                this.switchToSong(-1);
                this.previousSongButtonIsDown = false;
            }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.previousSongButtonIsDown = true; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.previousSongButtonIsDown = false; }.bind(this)
        }
    );

    this.nextSongButtonIsDown = undefined;
    this.nextSongButton = new Button(
        tabletScreenWidth - 83, tabletScreenHeight - 83, 83, 83,
        {
            mouseDown: function(x, y) { this.nextSongButtonIsDown = true; }.bind(this),
            mouseUp: function(x, y) {
                this.switchToSong(1);
                this.nextSongButtonIsDown = false;
            }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.nextSongButtonIsDown = true; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.nextSongButtonIsDown = false; }.bind(this)
        }
    );

    this.toggleAnimationEnd = undefined, this.toggleEnableAnimationEnd = undefined;
    this.toggleButton = new Button(
        293, 449, 112, 62,
        {
            mouseUp: function(x, y) {
                removeButton(this.toggleButton);
                this.toggled = !this.toggled;
                this.toggleAnimationEnd = performance.now() + MUSIC_PLAYER_TOGGLE_DURATION;
                this.switchToSong(0, true);
            }.bind(this)
        }
    );
}

MusicPlayerSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    tabletContext.fillStyle = "black";
    tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);

    // Draw the waveform
    // Based on this example https://developer.mozilla.org/en-US/docs/Web/API/AnalyserNode/getByteTimeDomainData
    tabletContext.save();
    tabletContext.translate(0, 30);
    function waveformDataToY(n) {
        return (n / 128) * (MUSIC_PLAYER_WAVEFORM_HEIGHT / 2);
    }
    musicPlayer.analyserNode.getByteTimeDomainData(this.dataArray);
    tabletContext.lineWidth = 2;
    tabletContext.strokeStyle = "#80FF80";
    tabletContext.beginPath();
    tabletContext.moveTo(0, waveformDataToY(this.dataArray[0]));
    for (let i = 1; i < this.bufferLength; i++) {
        tabletContext.lineTo(i * tabletScreenWidth / this.bufferLength, waveformDataToY(this.dataArray[i]));
    }
    tabletContext.stroke();
    tabletContext.restore();

    // Song box, number, & title
    tabletContext.save();
    tabletContext.translate(0, 356);
    tabletContext.fillStyle = "#D0D0D0";
    tabletContext.fillRect(0, 0, tabletScreenWidth, 83);
    tabletContext.strokeStyle = "black";
    tabletContext.lineWidth = 2;
    tabletContext.strokeRect(0, 0, tabletScreenWidth, 83);
    tabletContext.fillStyle = "black";
    tabletContext.font = "14px sans-serif";
    drawCenteredText(tabletContext, (this.currentSong + 1) + "/" + MUSIC_PLAYER_SONG_INFO.length, tabletScreenWidth / 2, 7, 14);
    tabletContext.font = "30px sans-serif";
    drawCenteredText(tabletContext, MUSIC_PLAYER_SONG_INFO[this.currentSong].name, tabletScreenWidth / 2, 26, 30);

    // Song position
    function formatTimestamp(n) {
        let minutes = Math.floor(n / 60);
        let seconds = Math.floor(n % 60);
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        return (minutes + ":" + seconds);
    }
    let currentSongPositionInfo = musicPlayer.currentSongPositionInfo();
    tabletContext.font = "14px sans-serif";
    drawCenteredText(tabletContext, formatTimestamp(currentSongPositionInfo.currentPosition) + "/" + formatTimestamp(currentSongPositionInfo.duration), tabletScreenWidth / 2, 62, 14);
    tabletContext.restore();

    // Song controls
    tabletContext.save();
    tabletContext.translate(0, 439);
    tabletContext.fillStyle = "#D0D0D0";
    tabletContext.fillRect(0, 0, tabletScreenWidth, 83);
    tabletContext.fillStyle = "black";
    tabletContext.beginPath();
    tabletContext.moveTo(61.5, 19);
    tabletContext.lineTo(61.5, 62);
    tabletContext.lineTo(30, 40.5);
    tabletContext.fill();
    tabletContext.fillRect(23.5, 19, 5, 43);
    tabletContext.beginPath();
    tabletContext.moveTo(634.5, 19);
    tabletContext.lineTo(634.5, 62);
    tabletContext.lineTo(666, 40.5);
    tabletContext.fill();
    tabletContext.fillRect(668, 19, 5, 43);
    if (this.previousSongButtonIsDown) {
        tabletContext.fillStyle = "rgba(0, 0, 0, 0.5)";
        tabletContext.fillRect(0, 0, 83, 83);
    } else if (this.nextSongButtonIsDown) {
        tabletContext.fillStyle = "rgba(0, 0, 0, 0.5)";
        tabletContext.fillRect(tabletScreenWidth - 83, 0, 83, 83);
    }
    tabletContext.strokeStyle = "black";
    tabletContext.lineWidth = 2;
    tabletContext.strokeRect(0, 0, 83, 83);
    tabletContext.strokeRect(83, 0, tabletScreenWidth - 2 * 83, 83);
    tabletContext.strokeRect(tabletScreenWidth - 83, 0, 83, 83);
    tabletContext.restore();

    // Variation on/off switcher
    if (timestamp >= this.toggleAnimationEnd) {
        this.toggleAnimationEnd = undefined;
        if (!this.leaving && this.toggleEnabled) {
            addButton(this.toggleButton);
        }
    }
    let toggleProgress = 0;
    if (this.toggleAnimationEnd !== undefined) {
        toggleProgress = (this.toggleAnimationEnd - timestamp) / MUSIC_PLAYER_TOGGLE_DURATION;
    }
    if (this.toggled) {
        toggleProgress = 1 - toggleProgress;
    }

    if (timestamp >= this.toggleEnableAnimationEnd) {
        this.toggleEnableAnimationEnd = undefined;
        if (!this.leaving && this.toggleEnabled) {
            addButton(this.toggleButton);
        }
    }
    let toggleEnableProgress = 0;
    if (this.toggleEnableAnimationEnd !== undefined) {
        toggleEnableProgress = (this.toggleEnableAnimationEnd - timestamp) / MUSIC_PLAYER_TOGGLE_ENABLE_DURATION;
    }
    if (this.toggleEnabled) {
        toggleEnableProgress = 1 - toggleEnableProgress;
    }

    tabletContext.save();
    tabletContext.translate(323, 439);
    tabletContext.fillStyle = "rgb(" + Math.round((1 - toggleProgress) * 127 + 128) + ",255," + Math.round((1 - toggleProgress) * 127 + 128) + ")";
    tabletContext.strokeStyle = "black";
    tabletContext.beginPath();
    tabletContext.arc(0, 40, 30, 3 * Math.PI / 2, Math.PI / 2, true);
    tabletContext.lineTo(50, 70);
    tabletContext.arc(50, 40, 30, Math.PI / 2, 3 * Math.PI / 2, true);
    tabletContext.lineTo(0, 10);
    tabletContext.fill();
    tabletContext.stroke();
    tabletContext.fillStyle = "rgb(" + Math.round(208 + 47 * toggleEnableProgress) + ", " + Math.round(208 + 47 * toggleEnableProgress) + "," + Math.round(208 + 47 * toggleEnableProgress) + ")";
    tabletContext.beginPath();
    tabletContext.arc(50 * toggleProgress, 40, 25, 0, 2 * Math.PI);
    tabletContext.fill();
    tabletContext.stroke();
    tabletContext.restore();

    drawStatusBar(tabletContext, "Music Player", true);
}

MusicPlayerSceneController.prototype.startTransitionIn = function(transitionType) {
    this.leaving = false;

    this.bufferLength = musicPlayer.analyserNode.frequencyBinCount;
    this.dataArray = new Uint8Array(this.bufferLength);
    this.currentSong = (musicPlayer.currentSong() === "menu-full") ? 0 : 6;

    this.previousSongButtonIsDown = false;
    this.nextSongButtonIsDown = false;

    this.toggleEnabled = false;
    this.toggled = true;
    this.toggleAnimationEnd = undefined;
    this.toggleEnableAnimationEnd = undefined;
}

MusicPlayerSceneController.prototype.finishTransitionIn = function(transitionType) {
    addButton(this.previousSongButton);
    addButton(this.nextSongButton);
}

MusicPlayerSceneController.prototype.startTransitionOut = function(transitionType) {
    this.leaving = true;
    removeButton(this.previousSongButton);
    removeButton(this.nextSongButton);
    removeButton(this.toggleButton);
}

MusicPlayerSceneController.prototype.finishTransitionOut = function(transitionType) {

}
