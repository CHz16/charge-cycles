// testredefines.js
// Redefinitions of some stuff to speed up loading by skipping the intro loading animation and not loading music

LOADING_PROGRESS_BAR_MAX_SPEED = 1;
LOADING_PROGRESS_BAR_FADEOUT_DURATION = 1;
LOADING_SEGMENT_TRANSITION_LENGTH = 1;
LOADING_LOGO_HOLD = 1;

GAME_VICTORY_ANIMATION_STATE_DURATIONS = [500, 500, 500, 500, 500, 500, 50, 250, 250];
GAME_VICTORY_ANIMATION_MIN_LOOPS = 0;

music = [{key: "menu-full", urls: ["menu-full.ogg", "menu-full.mp3"]}, {key: "ingame1-light", urls: ["ingame1-light.ogg", "ingame1-light.mp3"]}, {key: "ingame1-full", urls: ["ingame1-full.ogg", "ingame1-full.mp3"]}];
//music = [];
musicPlayer = {play: function() {}, stop: function() {}, setPlaybackRateOfCurrentChannels: function() {}, setVolume: function() {}, currentSong: function() { return undefined; }, currentSongPositionInfo: function() { return {currentPosition: 0, duration: 0} }, analyserNode: {frequencyBinCount: 2, getByteTimeDomainData: function(dataArray) { dataArray[0] = 0; dataArray[1] = 0; }}};
