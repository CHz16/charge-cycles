// manualscene.js
// Controller for the manual scene.

let MANUAL_PAGE_WIDTH = 328, MANUAL_PAGE_HEIGHT = 473;
let MANUAL_TURN_DURATION = 250;
let MANUAL_PAGES = [
    {
        title: "",
        rows: [
            {image: {key: "manual-1-1"}},
            {text: "Welcome to Ulka! We're thrilled that you've joined our global family of independent charge specialists."},
            {text: "This manual contains everything you need to know to operate our signature chargers. Please take some time to thoroughly review the information here, so that you can provide the best possible charging experience to your clients."}
        ]
    },
    {
        title: "Message From the Prez",
        rows: [
            {text: "On behalf of myself and everyone here at Ulka, thank you for bringing your talent and energy to the #1 personal charging company on the planet."},
            {text: "Even though power distribution is free to our homes, millions around the world still trust us to provide them with a personalized charging experience. Our chargers are loved for the intense sensation they provide over traditional wall power."},
            {text: "We're glad to have you with us, and we hope to see you for many years to come."},
            {text: "\u286f\u2824\u28bb\u28af\u2893\u283c\u28ea\u28ea\u286f", image: {key: "manual-2-1"}}
        ]
    },
    {
        title: "Chargers",
        rows: [
            {text: "These little things are chargers. They come in all sizes and generate electricity through friction by constantly moving.", image: {key: "manual-3-1"}},
            {text: "The software will tell you what chargers are available in the package your client purchased."},
            {image: {key: "manual-3-2", caption: "This client has paid for two chargers of length 2 and one of length 3."}}
        ]
    },
    {
        title: "Insertion",
        rows: [
            {text: "To place a charger inside your client, click the square where the head should be, move the mouse to draw the body, and click again to finish."},
            {text: "To take a charger out, just click it."},
            {image: {key: "manual-4-1", carouselLength: 5, captions: ["We click here to start a new charger.", "The highlighted squares show the possible directions we can move to extend the charger.", "If a charger of length 2 is available, clicking now would place it here.", "The maximum length available in this example is 3, so we can't extend this charger any further.", "After clicking, we've placed the charger and it no longer appears transparent."]}},
            {note: true, text: "You can also place a charger by clicking and dragging from the head."}
        ]
    },
    {
        title: "Charge Ahead",
        rows: [
            {text: "Chargers move straight, one square at a time, until they hit something."},
            {image: {key: "manual-5-1", carouselLength: 3, captions: ["Each charger here will move forward three squares before hitting something.", "Each charger here will move forward three squares before hitting something.", "Each charger will now turn. (See the next page.)"]}},
            {note: true, text: "Tails retract before the heads move, so a charger can move into the square occupied by another charger's tail.", image: {key: "manual-5-2", carouselLength: 4}}
        ]
    },
    {
        title: "Detours",
        rows: [
            {text: "If a charger can't move straight ahead, it will first try to turn right and move."},
            {text: "If it can't do that either, it will turn left and move instead."},
            {image: {key: "manual-6-1", carouselLength: 4, captions: ["This charger will turn right every time it hits a wall and follow the arrows in a loop.", "This charger will turn left every time it hits a wall and follow the arrows in a loop.", "These chargers will both turn right after hitting each other and go in opposite directions.", "With this extra wall on its right, the charger on the left will now turn left instead and both will go in the same direction."]}}
        ]
    },
    {
        title: "Energizing",
        rows: [
            {text: "Click the play button to start the chargers. Once they're moving, the pause button will stop them, and the play button will start them again. The restart button will reset everything and let you edit again.", image: {key: "manual-7-1"}},
            {text: "When paused, you can use the arrow buttons to step backward or forward one turn. The current turn will be shown with a black border in the chart above the buttons.", image: {key: "manual-7-2"}},
            {image: {key: "manual-7-3", caption: "You can also click a box to go straight to that turn."}}
        ]
    },
    {
        title: "Premature Termination",
        rows: [
            {text: "If a charger can't move forward, right, or left, the software will stop the chargers and show you which one is blocked.", image: {key: "manual-8-1"}},
            {text: "If two chargers try to move into the same square, the software will stop the chargers and show you which two would collide and where.", image: {key: "manual-8-2"}},
            {note: true, text: "Forcing a charger to move after the software has detected a problem will void its warranty!"}
        ]
    },
    {
        title: "Charging Their Batteries",
        rows: [
            {text: "As you know, these are the standard batteries that we all have inside us.", image: {key: "manual-9-1"}},
            {text: "Chargers will charge any batteries they touch, but remember that they move constantly. They won't stop just because they're charging a battery.", image: {key: "manual-9-2"}},
            {text: "Your goal is to provide a consistent charge to your clients by arranging chargers so that their movements form a stable loop, where every battery is being charged by at least one charger on every step of the loop."}
        ]
    },
    {
        title: "Loop Feedback",
        rows: [
            {text: "Once a loop is detected, it will be highlighted in the turn chart."},
            {image: {key: "manual-10-1", carouselLength: 5, carouselLoopStart: 1, caption: "Turns where every battery is being charged will have an electrical icon."}},
            {note: true, text: "Unless your client has purchased the Extended service, chargers will be limited to fifty turns to find a cycle."}
        ]
    }
];

function ManualSceneController() {
    this.addTurnButtons = function() {
        if (this.currentPage != 0) {
            addButton(this.turnLeftButton);
        }
        if (this.currentPage != MANUAL_PAGES.length - 2) {
            addButton(this.turnRightButton);
        }
    }

    this.nextCarouselID = 0;
    this.drawPage = function(context, pageNumber) {
        let page = MANUAL_PAGES[pageNumber];

        // Preprocess the page to find line breaks and all that
        if (!page.analyzed) {
            page.analyzed = true;
            let y = (page.title !== "") ? 40 : 0;
            for (let i = 0; i < page.rows.length; i++) {
                let row = page.rows[i];
                let baseWidth = MANUAL_PAGE_WIDTH - (row.note ? 20 : 0);
                row.y = y;

                // Generate the image keys if they need generation
                if (row.image !== undefined && row.image.keys === undefined) {
                    if (row.image.carouselLength === undefined) {
                        row.image.keys = [row.image.key + "-1"];
                    } else {
                        let keys = [];
                        for (let i = 0; i < row.image.carouselLength; i++) {
                            keys.push(row.image.key + "-" + (i + 1));
                        }
                        row.image.keys = keys;
                    }
                } else if (row.image !== undefined && row.image.keys !== undefined) {
                    row.image.carouselLength = row.image.keys.length;
                }

                // Precalculate some positional stuff
                // We also turn standalone images into a "one-image carousel" for code simplicity
                if (row.image !== undefined) {
                    row.image.id = this.nextCarouselID;
                    this.nextCarouselID += 1;
                    row.image.width = imagePreloader.getImage(row.image.keys[0]).width;
                    row.image.height = imagePreloader.getImage(row.image.keys[0]).height;
                    row.image.left = (row.text !== undefined)
                        ? (MANUAL_PAGE_WIDTH - row.image.width - (row.note ? 10 : 0))
                        : Math.round((MANUAL_PAGE_WIDTH - row.image.width) / 2);
                    row.image.top = row.y + (row.note ? 35 : 0);
                    row.image.rawHeight = row.image.height;

                    if (row.image.carouselLength === undefined) {
                        row.image.carouselLength = 1;
                    }
                    if (row.image.carouselLoopStart === undefined) {
                        row.image.carouselLoopStart = 0;
                    }
                    this.currentCarouselPages[row.image.id] = 0;
                    if (row.image.caption !== undefined) {
                        row.image.captions = Array(row.image.carouselLength).fill(row.image.caption);
                    }
                }

                // Add line breaks to the captions
                // We do this first because this changes the image height, which the word wrapping in the text line break insertion needs
                if (row.image !== undefined && row.image.captions !== undefined) {
                    context.font = "italic 16px serif";
                    let captionsLines = [], maxCaptionHeight = 0;
                    for (let j = 0; j < row.image.captions.length; j++) {
                        if (row.image.captions[j] === "") {
                            captionsLines.push([]);
                            continue;
                        }

                        let captionWidth = (row.text !== undefined) ? row.image.width : (MANUAL_PAGE_WIDTH - (row.note ? 20 : 0));
                        let captionLines = [], captionWords = row.image.captions[j].split(" "), captionLine = captionWords[0];
                        for (let k = 1; k < captionWords.length; k++) {
                            let extendedCaptionLine = captionLine + " " + captionWords[k];
                            let metrics = context.measureText(extendedCaptionLine);
                            if (metrics.width > captionWidth) {
                                captionLines.push(captionLine);
                                captionLine = captionWords[k];
                            } else {
                                captionLine = extendedCaptionLine;
                            }
                        }
                        captionLines.push(captionLine);
                        maxCaptionHeight = Math.max(maxCaptionHeight, 21 * captionLines.length - 5);
                        captionsLines.push(captionLines);
                    }
                    row.image.captionsLines = captionsLines;
                    row.image.height += maxCaptionHeight;
                }

                // Add line breaks to the text
                context.font = "20px serif";
                let lines = [];
                if (row.text !== undefined) {
                    let words = row.text.split(" "), line = words[0];
                    for (let j = 1; j < words.length; j++) {
                        let width = baseWidth;
                        if (row.image !== undefined && (lines.length * 25) <= row.image.height + 5) {
                            width -= row.image.width + 10;
                        }

                        let extendedLine = line + " " + words[j];
                        let metrics = context.measureText(extendedLine);
                        if (metrics.width > width) {
                            lines.push(line);
                            line = words[j];
                        } else {
                            line = extendedLine;
                        }
                    }
                    lines.push(line);
                }
                row.lines = lines;

                row.height = Math.max(lines.length * 25, (row.image !== undefined ? row.image.height : 0) + 5) + (row.note ? 25 + 12 : 0);
                y += row.height + 10 + (row.note ? 5 : 0);
            }
        }

        // Title & page number
        context.fillStyle = "black";
        context.font = "bold 30px serif";
        drawCenteredText(context, page.title, MANUAL_PAGE_WIDTH / 2, 0, 30);
        context.font = "16px serif";
        drawCenteredText(context, pageNumber + 1, MANUAL_PAGE_WIDTH / 2, MANUAL_PAGE_HEIGHT - 16, 16);

        // Contents
        context.strokeStyle = "black";
        context.lineWidth = 2;
        for (let i = 0; i < page.rows.length; i++) {
            let row = page.rows[i];
            let y = row.y;

            // Draw the note inlay
            if (row.note) {
                context.fillStyle = "white";
                context.beginPath();
                context.rect(1, y + 1, MANUAL_PAGE_WIDTH - 2, row.height);
                context.fill();
                context.stroke();

                context.fillStyle = "black";
                context.font = "bold 20px serif";
                drawCenteredText(context, "Note:", MANUAL_PAGE_WIDTH / 2, y + 10, 20);

                y += 25;
            }

            // Draw the text
            context.fillStyle = "black";
            context.font = "20px serif";
            for (let j = 0; j < row.lines.length; j++) {
                drawText(context, row.lines[j], (row.note ? 10 : 0), y + 25 * j + (row.note ? 10 : 0), 20);
            }

            // Draw the image
            if (row.image !== undefined) {
                let captionCenter, captionTop = y + (row.note ? 10 : 0) + row.image.rawHeight + 5;
                if (row.text !== undefined) {
                    // If the image is in the same row as text, then pin it right
                    if (row.image.captionsLines !== undefined) {
                        captionCenter = MANUAL_PAGE_WIDTH - (row.note ? 10 : 0) - row.image.width / 2;
                    }
                } else {
                    // Otherwise, center it
                    if (row.image.captionsLines !== undefined) {
                        captionCenter = MANUAL_PAGE_WIDTH / 2;
                    }
                }

                // Image & caption
                context.drawImage(imagePreloader.getImage(row.image.keys[this.currentCarouselPages[row.image.id]]), row.image.left, row.image.top);
                if (row.image.captionsLines !== undefined) {
                    let captionLines = row.image.captionsLines[this.currentCarouselPages[row.image.id]];
                    context.font = "italic 16px serif";
                    for (let j = 0; j < captionLines.length; j++) {
                        drawCenteredText(context, captionLines[j], captionCenter, captionTop + j * 21, 16);
                    }
                }

                // Carousel position & buttons
                if (row.image.carouselLength > 1) {
                    let carouselDotsLeft = Math.round(row.image.left + row.image.width / 2 - (row.image.carouselLength * 15 - 5) / 2), carouselDotsTop = row.image.top + row.image.rawHeight - 10 - 5;
                    for (let j = 0; j < row.image.carouselLength; j++) {
                        context.drawImage(imagePreloader.getImage(j == this.currentCarouselPages[row.image.id] ? "carousel-selected" : "carousel-unselected"), carouselDotsLeft + 15 * j, carouselDotsTop);
                    }

                    context.fillStyle = "rgba(255, 255, 255, 0.75)";
                    if (this.hoveredCarouselButton == row.image.id + "-left") {
                        context.fillRect(row.image.left, row.image.top, 25, row.image.rawHeight);
                        context.drawImage(imagePreloader.getImage("carousel-left"), row.image.left + 8, row.image.top + Math.round((row.image.rawHeight - 13) / 2));
                    } else {
                        context.fillRect(row.image.left, row.image.top, 10, row.image.rawHeight);
                        context.drawImage(imagePreloader.getImage("carousel-left"), row.image.left + 1, row.image.top + Math.round((row.image.rawHeight - 13) / 2));
                    }
                    if (this.hoveredCarouselButton == row.image.id + "-right") {
                        context.fillRect(row.image.left + row.image.width - 25, row.image.top, 25, row.image.rawHeight);
                        context.drawImage(imagePreloader.getImage("carousel-right"), row.image.left + row.image.width - 25 + 8, row.image.top + Math.round((row.image.rawHeight - 13) / 2));

                    } else {
                        context.fillRect(row.image.left + row.image.width - 10, row.image.top, 10, row.image.rawHeight);
                        context.drawImage(imagePreloader.getImage("carousel-right"), row.image.left + row.image.width - 10 + 1, row.image.top + Math.round((row.image.rawHeight - 13) / 2));
                    }
                }
            }
        }
    }

    this.turnDirection = undefined, this.turnEnd = undefined;
    this.turnPage = function(direction) {
        if (direction == -1) {
            this.currentPage -= 2;
        }
        this.turnDirection = direction;
        this.turnEnd = performance.now() + MANUAL_TURN_DURATION;
        this.activeTurnButton = undefined;
        removeButton(this.turnLeftButton);
        removeButton(this.turnRightButton);
        removeButtons(this.carouselButtons);
        this.carouselButtons = [];
    }
    this.activeTurnButton = undefined;
    this.turnLeftButton = new Button(
        0, tabletScreenHeight - 30, 30, 30,
        {
            mouseDown: function(x, y) { this.activeTurnButton = 0; }.bind(this),
            mouseUp: function(x, y) { this.turnPage(-1); }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeTurnButton = 0; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.activeTurnButton = undefined; }.bind(this)
        }
    );
    this.turnRightButton = new Button(
        tabletScreenWidth - 30, tabletScreenHeight - 30, 30, 30,
        {
            mouseDown: function(x, y) { this.activeTurnButton = 1; }.bind(this),
            mouseUp: function(x, y) { this.turnPage(1); }.bind(this),
            mouseEnter: function(x, y, mouseIsDown) { if (mouseIsDown) { this.activeTurnButton = 1; } }.bind(this),
            mouseLeave: function(x, y, mouseIsDown) { this.activeTurnButton = undefined; }.bind(this)
        }
    );

    this.currentCarouselPages = [], this.carouselButtons = [], this.hoveredCarouselButton = undefined;
    this.addCarouselButtons = function() {
        let pages = [this.currentPage, this.currentPage + 1], transformOffsets = [{x: 11, y: 38}, {x: tabletScreenWidth / 2 + 10 - 1, y: 38}];
        for (let i = 0; i < pages.length; i++) {
            for (let j = 0; j < MANUAL_PAGES[pages[i]].rows.length; j++) {
                let row = MANUAL_PAGES[pages[i]].rows[j];
                if (row.image !== undefined && row.image.carouselLength > 1) {
                    this.carouselButtons.push(new Button(
                        row.image.left + transformOffsets[i].x, row.image.top + transformOffsets[i].y, 25, row.image.rawHeight,
                        {
                            mouseUp: function(x, y) { this.currentCarouselPages[row.image.id] = (this.currentCarouselPages[row.image.id] - 1 + row.image.carouselLength) % row.image.carouselLength; }.bind(this),
                            mouseEnter: function(x, y) { this.hoveredCarouselButton = row.image.id + "-left"; }.bind(this),
                            mouseLeave: function(x, y) { this.hoveredCarouselButton = undefined; }.bind(this),
                        }
                    ));
                    this.carouselButtons.push(new Button(
                        row.image.left + transformOffsets[i].x + row.image.width - 25, row.image.top + transformOffsets[i].y, 25, row.image.rawHeight,
                        {
                            mouseUp: function(x, y) {
                                if (this.currentCarouselPages[row.image.id] == row.image.carouselLength - 1) {
                                    this.currentCarouselPages[row.image.id] = row.image.carouselLoopStart;
                                } else {
                                    this.currentCarouselPages[row.image.id] += 1;
                                }
                            }.bind(this),
                            mouseEnter: function(x, y) { this.hoveredCarouselButton = row.image.id + "-right"; }.bind(this),
                            mouseLeave: function(x, y) { this.hoveredCarouselButton = undefined; }.bind(this),
                        }
                    ));
                }
            }
        }
        addButtons(this.carouselButtons);
    }

    this.currentPage = 0;
    this.leaving = false;
}

ManualSceneController.prototype.draw = function(timestamp, dt, mainContext, tabletContext) {
    // Finalize the page turn
    if (this.turnEnd !== undefined && timestamp >= this.turnEnd) {
        if (this.turnDirection == 1) {
            this.currentPage += 2;
        }
        if (!this.leaving) {
            this.addTurnButtons();
            this.addCarouselButtons();
        }
        this.turnDirection = undefined;
        this.turnEnd = undefined;
    }

    // Draw the background and page divider
    tabletContext.fillStyle = "#F0F0F0";
    tabletContext.fillRect(0, 0, tabletScreenWidth, tabletScreenHeight);
    tabletContext.strokeStyle = "#B0B0B0";
    tabletContext.lineWidth = 2;
    tabletContext.beginPath();
    tabletContext.moveTo(tabletScreenWidth / 2, 30);
    tabletContext.lineTo(tabletScreenWidth / 2, tabletScreenHeight);
    tabletContext.stroke();

    // Calculate how far we are into the turn
    let turnProgress = 0;
    if (this.turnEnd !== undefined) {
        turnProgress = clamp((this.turnEnd - timestamp) / MANUAL_TURN_DURATION, 0, 1);
        if (this.turnDirection == 1) {
            turnProgress = 1 - turnProgress;
        }
    };

    // Left page
    tabletContext.save();
    tabletContext.translate(11, 38);
    tabletContext.beginPath();
    tabletContext.rect(0, 0, MANUAL_PAGE_WIDTH * ((turnProgress <= 0.5) ? 1 : 2 - turnProgress * 2), MANUAL_PAGE_HEIGHT);
    tabletContext.clip();
    this.drawPage(tabletContext, this.currentPage);
    tabletContext.restore();

    // Right page
    if (turnProgress <= 0.5) {
        tabletContext.save();
        tabletContext.translate(tabletScreenWidth / 2 + 10 - 1, 38);
        tabletContext.beginPath();
        tabletContext.rect(0, 0, MANUAL_PAGE_WIDTH * (1 - 2 * turnProgress), MANUAL_PAGE_HEIGHT);
        tabletContext.clip();
        this.drawPage(tabletContext, this.currentPage + 1);
        tabletContext.restore();
    }

    // New pages
    if (this.turnEnd !== undefined) {
        // New left page
        if (turnProgress >= 0.5) {
            let newLeftWidth = MANUAL_PAGE_WIDTH * (turnProgress * 2 - 1);
            tabletContext.save();
            tabletContext.translate(11, 38);
            tabletContext.beginPath();
            tabletContext.rect(MANUAL_PAGE_WIDTH - newLeftWidth, 0, newLeftWidth, MANUAL_PAGE_HEIGHT);
            tabletContext.clip();
            this.drawPage(tabletContext, this.currentPage + 2);
            tabletContext.restore();
        }

        // New right page
        let newRightWidth = MANUAL_PAGE_WIDTH * ((turnProgress >= 0.5) ? 1 : 2 * turnProgress);
        tabletContext.save();
        tabletContext.translate(tabletScreenWidth / 2 + 10 - 1, 38);
        tabletContext.beginPath();
        tabletContext.rect(MANUAL_PAGE_WIDTH - newRightWidth, 0, newRightWidth, MANUAL_PAGE_HEIGHT);
        tabletContext.clip();
        this.drawPage(tabletContext, this.currentPage + 3);
        tabletContext.restore();
    }

    // Turn buttons
    if (this.currentPage != 0) {
        tabletContext.drawImage(imagePreloader.getImage("pageturn-left" + (this.activeTurnButton === 0 ? "-pressed" : "")), 3, 495);
    }
    if (this.currentPage < MANUAL_PAGES.length - 4 || (this.currentPage == MANUAL_PAGES.length - 4 && this.turnDirection === undefined)) {
        tabletContext.drawImage(imagePreloader.getImage("pageturn-right" + (this.activeTurnButton === 1 ? "-pressed" : "")), 669, 495);
    }

    drawStatusBar(tabletContext, "Manual", true);
}

ManualSceneController.prototype.startTransitionIn = function(transitionType) {
    this.activeTurnButton = undefined;
    this.turnDirection = undefined;
    this.turnEnd = undefined;
    this.addTurnButtons();
    this.addCarouselButtons();
}

ManualSceneController.prototype.finishTransitionIn = function(transitionType) {

}

ManualSceneController.prototype.startTransitionOut = function(transitionType) {
    this.leaving = true;
    removeButton(this.turnLeftButton);
    removeButton(this.turnRightButton);
    removeButtons(this.carouselButtons);
}

ManualSceneController.prototype.finishTransitionOut = function(transitionType) {
    this.leaving = false;
}
